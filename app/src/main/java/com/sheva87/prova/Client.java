package com.sheva87.prova;

import android.content.Context;
import android.location.Location;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

import com.graphhopper.GraphHopper;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Client {
    private final GUIUtente gui;
    protected Dijkstra router;
    protected GPS locationManager;
    private GraphHopper hopper=null;
    public double lat=0,lon=0,prevlat=0,prevlon=0;
    DataWay prevway,attway;
    public int prevEdge=-1;
    public int idEdge=-1;
    public Client(GUIUtente context){
        gui = context;

    }
    public void creaGPS(){
        Log.i("GPS", "creaGPS");
        locationManager = new GPS(gui);
    }

    public void getMap(String chosedMap){
        CSI csi = new CSI(gui);
        csi.execute(chosedMap, "mappa");
    }
    public void unzip(String zipFilePath){
        gui.setPDMessage("2/7 : Decompressione e salvataggio mappa");
        new AsyncTask<Object,Void,String>(){

            @Override
            protected String doInBackground(Object... params) {
                String path=(String)params[0];
                Unzipper unzipper = new Unzipper(path, Setup.appFolder);
                unzipper.unzip();
                Log.i("INFO", "file decompresso!!");
                return null;
            }
            protected void onPostExecute(String result){
                gui.saveMap();
            }
        }.execute(zipFilePath);
    }
    public void getPesiDin(){
        if(gui.flagPrimoAvvio)  gui.setPDMessage("4/7 : Recupero pesi dinamici dal server");
        CSI csi = new CSI(gui);
        csi.execute(null, "pesiDin");
    }

    public void loadGraph(){
        router = new Dijkstra(gui);
        router.execute(0);

    }

    public void finalPath(double lat, double lon, double[][] rac){
        router = new Dijkstra(gui,hopper);
        router.execute(1, lat, lon, rac);
    }
    public void saveGraph(GraphHopper h){
        hopper=h;
    }

    public void putPos(int[] idMapping){
        if(idMapping!=null) {
            gui.toast("put("+idMapping[0]+","+idMapping[1]+")");
            CSI csi = new CSI(gui);
            csi.execute(null, "putPos", idMapping[0], idMapping[1]);
        }
    }
    public void putPos2(int[] idMapping){
        if(idMapping!=null) {
            gui.toast("put("+idMapping[0]+","+idMapping[1]+")");
            CSI csi = new CSI(gui);
            csi.execute(null, "putPos2", idMapping[0], idMapping[1]);
        }
    }
    //leggo il MAC address dal dispositivo e lo salvo in un file di testo
    public String findMAC(){
        File file = new File(Setup.appFolder, Setup.MACfile);
        File destDir = new File (Setup.appFolder);
        if(!destDir.exists()){    destDir.mkdir();        }
        try {
            if(!file.exists()) {
                    WifiManager manager=(WifiManager) gui.getSystemService(Context.WIFI_SERVICE);
                    WifiInfo info=manager.getConnectionInfo();
                    String address=info.getMacAddress();
                    generaFile(address);
            }
            BufferedReader br=new BufferedReader(new FileReader(file));
            String line=br.readLine();
            br.close();
            return line;
        } catch (IOException e) {
            e.printStackTrace();
            gui.alert("Errore nella lettura del file MAC");
            return null;
        }


    }

    //dall'indirizzo genera le coordinate
    public double[] geocoding(String path){
        CSI csi = new CSI(gui);
        csi.execute(path,"geocoding");
        return null;
    }

    //genero il file di testo contenente il MAC Address
    public void generaFile(String s){
        try{
            File file = new File(Setup.appFolder, Setup.MACfile);
            FileWriter writer = new FileWriter(file);
            writer.append(s);
            writer.flush();
            writer.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public void fakeGps(double plat, double plon) {
        locationManager.platset(plat);
        locationManager.plonset(plon);
        //locationManager.start();
    }
    public void setPos(double latt, double lonn){
        prevlat=lat;
        prevlon=lon;
        lat=latt;
        lon=lonn;
        /*
        if (attDist()>1000){
            Tiles t=new Tiles(gui);
            t.scaricaTiles(lat, lon, 18);
            gui.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    gui.myWebView.loadUrl("javascript:setTilesFolder('" + Setup.tilesFolder + "')");
                    gui.myWebView.loadUrl("javascript:initPos(" + lon + ", " + lat + ")");
                }
            });
            }
            */
    }
    /*
    public float attDist(){
        if(prevlat!=0 && prevlon!=0)    return distance(lat,lon,prevlat,prevlon);
        else return 0;
    }*/

    //Serve a calcolare la distanza tra due punti di coordinate geografiche
    public float distance(double lat1,double lon1,double lat2,double lon2){
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }
    //Valuta se la distanza dall'ultimo punto è maggiore di 10 m

    public boolean testDist(float m){
        if(prevlat==0 && prevlon==0){
            testEdge();
            return true;
        }/*
        float attDist=distance(lat,lon,prevlat,prevlon);
        Log.i("GPS","distance="+attDist);
        if (attDist>m){
            testEdge();
            return true;
        }
        else return false;
        */
        double difflat=Math.abs(lat-prevlat);
        double difflon=Math.abs(lon-prevlon);
        if(difflat<0.00002 && difflon<0.00002)  return false;
        else{
            testEdge();
            return true;
        }
    }

    //Cerco l'edge in cui mi trovo e se è diverso da quello precedente faccio partire richiesta di registrazione al server
    public void testEdge(){
        if(gui.graphLoaded) {
            prevEdge=idEdge;
            idEdge = router.localEdge(lat, lon);
            Log.i("INFO", "idEdge rilevato " + idEdge);

            prevway = attway;
            if( idEdge!=-1) attway = gui.getway(idEdge);
            else    attway=null;
            if(attway!=null) {
                if(prevway!=null) {
                    if (attway.idOSMend != prevway.idOSMend && attway.idOSMin != prevway.idOSMin) {
                        int[] way = new int[2];
                        way[0] = attway.idOSMin;
                        way[1] = attway.idOSMend;
                        putPos2(way);
                    }
                } else {
                    int[] way = new int[2];
                    way[0] = attway.idOSMin;
                    way[1] = attway.idOSMend;
                    putPos2(way);}
            }else{
                //caso edge non mappato ma esistente per graphhopper
                newPath();
            }

        }
    }
    //se ho cambiato edge allora ricalcolo il percorso (modalità GPS e fakeGPS
    public void newPath(){
        if(idEdge!=(-1) && idEdge!=prevEdge){
            if(gui.salvamiOn) gui.percorso();
        }
    }
    public void resetLoc(){
        lat=0;
        lon=0;
        prevlat=0;
        prevlon=0;
        prevway=null;
        attway=null;
        prevEdge=-1;
        idEdge=-1;
    }
}
