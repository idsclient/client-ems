package com.sheva87.prova;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;


public class CSI extends AsyncTask<Object, Void, String> {

    private final GUIUtente context;
    private HttpRequest httpRequest = new HttpRequest();
    private int error;
    private String csiFlag=null;
    private String zipFilePath=null;
    private String place;

    public CSI(GUIUtente c) {
        error=0;
        context = c;
    }


    protected String doInBackground(Object... params) {
        csiFlag = (String)params[1];
        if(checkConnection()) {
            if(csiFlag.equals("mappa")) {
                String s=(String)params[0];
                getMap(s);
                return "done";
            }
            if(csiFlag.equals("pesiDin")) {
                getPesiDin();
                return "done";
            }
            if(csiFlag.equals("putPos")) {
                int id1=(int)params[2];
                int id2=(int)params[3];
                putPos(id1, id2);
                return "done";
            }
            if(csiFlag.equals("putPos2")) {
                int id1=(int)params[2];
                int id2=(int)params[3];
                putPos(id1,id2);
		return "done";
	    }
            if(csiFlag.equals("geocoding")){
                geocode((String)params[0]);
                return "done";
            }
            return "done";
        }else{
            error=1;
            return "error";
        }
    }


    protected void onPostExecute(String result){
        switch(error){
            case 0: //nessun errore
                if (csiFlag.equals("mappa")){
                    context.appClient.unzip(zipFilePath);
                }
                if(csiFlag.equals("pesiDin")) {
                    context.savePesiDin(httpRequest.response);
                }
                if(csiFlag.equals("putPos2")) {
                    context.appClient.newPath();
                }
		        if(csiFlag.equals("geocoding")){
			        if(httpRequest.geocode){
            			//context.wai.getTAPPosition(context.geocodePos[0],context.geocodePos[1]);
                        if(place==null) context.alert("Indirizzo non trovato!");
                        else {
                            context.myWebView.loadUrl("javascript:centerView(" + context.geocodePos[0] + "," + context.geocodePos[1] + ")");
                            context.alert(place);
                            httpRequest.geocode = false;
                        }
        		}
		}
                break;
            case 1: //no connessione
                if(!context.flagPrimoAvvio){
                    context.toast("Errore: nessuna connessione ad Internet attiva!");
                    context.dismissPD();
                    //context.posizionamento();
                    context.calcPath();
                }else{
                    context.alert("Errore: nessuna connessione ad Internet attiva!");
                    context.dismissPD();
                }
                break;
            case 2: //errore connessione col server per la mappa
                context.alert("Errore nella connessione con il server (mappa)!");
                context.dismissPD();
                break;
            case 3: //errore connessione col server per i pesiDin
                if(context.flagPrimoAvvio) context.alert("Errore nella connessione con il server (pesiDin) !");
                else    context.toast("Errore nella connessione con il server (pesiDin) !");
                context.dismissPD();
                context.calcPath();
                //context.posizionamento();
                break;
            case 4: //errore connessione col server per il put
                context.alert("Errore nella connessione con il server (put) !");
                if(csiFlag.equals("putPos2")) {
                    context.appClient.newPath();
                }
                break;
        }
    }


    protected void getMap(String chosedmap) {

        try {
            httpRequest.sendRequest("GET", Setup.urlMap + chosedmap + "/");
            zipFilePath=httpRequest.getZipfilepath();
        } catch (Exception e) {
            error=2;
            Log.i("ERROR","ERRORE connessione al server");
            e.printStackTrace();

        }
    }
    public boolean checkConnection(){
        ConnectivityManager cm =(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    protected void getPesiDin(){
        try {
            httpRequest.sendRequest("GET", Setup.urlPesi);
            context.useLOS=true;

        } catch (Exception e) {
            e.printStackTrace();
            error=3;
            context.useLOS=false;
        }
    }
    public void putPos(int id1, int id2){
        try {
            httpRequest.getIdMapping(id1, id2);
            httpRequest.sendRequest("PUT", Setup.urlPost + Setup.MAC);
        } catch (Exception e) {
            e.printStackTrace();
            error=4;
        }
    }

    public void geocode(String path){
        httpRequest.geocode = true;
        try {
            httpRequest.sendRequest("GET", path);
            double[] ris = new double[2];


            Log.i("Response: ",httpRequest.response);
            //JSONObject object = new JSONObject(httpRequest.response);
            JSONArray arr= (JSONArray) new JSONTokener(httpRequest.response).nextValue();

            for (int i=0;i<arr.length();i++) {
                JSONObject obj=arr.getJSONObject(i);
                ris[0]=obj.getDouble("lon");
                ris[1]=obj.getDouble("lat");
                place=obj.getString("display_name");
                //Log.i("INFO NODO1=", String.valueOf(obj.getInt("idnode1")));
            }

            context.geocodePos = ris;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}