package com.sheva87.prova;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

/**
 * Created by Simone on 09/07/15.
 */
public class SceltaTestMode extends DialogFragment {
    String[] choices;
    Context context;
    int selected;
    double plat,plon;

    public SceltaTestMode(double latt, double lonn, Context c) {
        plat=latt;
        plon=lonn;
        context=c;
        choices=new String[2];
        choices[0]="GPS";
        choices[1]="Simulazione";
        selected=0;


    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Set the dialog title
        builder.setTitle("Scegli se usare il GPS o una simulazione");
        // Specify the list array, the items to be selected by default (null for none),
        // and the listener through which to receive callbacks when items are selected
        builder.setSingleChoiceItems(choices, 0, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                selected=which;
            }
        });
        // Set the action buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                switch(selected){
                    case 0:
                        ((GUIUtente) context).hideImpostaPos();
                        Setup.gpsFakeMode=true;
                        ((GUIUtente)context).switchChosed();
                        ((GUIUtente) context).appClient.locationManager.checkGeolocation();
                        ((GUIUtente) context).appClient.fakeGps(plat, plon);
                        break;
                    case 1:
                        ((GUIUtente) context).hideImpostaPos();
                        Setup.gpsFakeMode=false;
                        ((GUIUtente)context).switchChosed();
                        ((GUIUtente)context).percorso();
                        break;

                }
            }
        })
                .setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(context, "ANNULLA", Toast.LENGTH_LONG).show();
                        ((GUIUtente)context).resetPercorso();

                    }
                });

        return builder.create();
    }
}
