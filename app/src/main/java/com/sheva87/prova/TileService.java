package com.sheva87.prova;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by Sheva87 on 26/06/2015.
 */
public class TileService extends IntentService {
    private String folder=Setup.tilesFolder;
    public ProgressDialog pd;
    public Handler handler;
    public TileService() {
        super("TileService");
        Log.i("Service", "Service creato!!!");
        handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                pd=new ProgressDialog(getApplicationContext());
                pd.setMessage("Scaricamento nuove tiles...");
                //Toast.makeText(getApplicationContext(), "Tiles scaricate 100%", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String url=intent.getStringExtra("url");
        Log.i("tiles", url);
        Bitmap b = getBitmapFromURL(url);
        if (b == null) {
            Log.i("tiles", "Bitmap NULL");
            return;
        }
        int z=intent.getIntExtra("z",0);
        int x=intent.getIntExtra("x",0);
        int y=intent.getIntExtra("y",0);
        //cartella Z
        String pathz=z+"";
        File fz=new File(folder,pathz);
        if(!fz.exists())    fz.mkdir();
        //cartella X
        String pathx=x+"";
        File fx=new File(fz.getPath(),pathx);
        if(!fx.exists())    fx.mkdir();
        //tile Y
        String pathy=y+".png";
        File fy = new File(fx.getPath(), pathy);
        if(!fy.exists())    storeImage(b, fy);
        else    Log.i("tiles", "Tile gia' esistente!! /"+z+"/"+x+"/"+y+".png");

        return;
    }

    //scarico la tile dall'url
    public Bitmap getBitmapFromURL(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Log.i("tileCode","connection.getResponseCode()"+connection.getResponseCode());
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    //salvo la tile scaricata
    private void storeImage(Bitmap image, File f) {
        File pictureFile = f;

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("tiles", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("tiles", "Error accessing file: " + e.getMessage());
        }
    }
    @Override
    public void onDestroy() {
        Log.i("tiles", "Service onDestroy()");
        //Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "Tiles scaricate 100%", Toast.LENGTH_LONG).show();
            }
        });
    }

}
