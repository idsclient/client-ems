package com.sheva87.prova;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.AlgorithmOptions;
import com.graphhopper.routing.util.AllEdgesIterator;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.util.Weighting;
import com.graphhopper.routing.util.WeightingMap;
import com.graphhopper.storage.GraphStorage;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.PointList;
import com.graphhopper.util.StopWatch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class Dijkstra extends AsyncTask<Object,Void,String>{

    private GraphHopper hopper;
    private GUIUtente gui;
    private int flag;
    float time;
    private GHRequest req;
    private GHResponse[] resp=null;
    private int nresp=0;
    private GHResponse minPath=null;
    private int nMinPath=0;
    private double[][] punti;   //punti del percorso calcolato
    private ArrayList<ArrayList<Double>> ris=null;  //punti filtrati del percorso calcolato


    public Dijkstra(GUIUtente context){
        gui = context;
    }
    public Dijkstra(GUIUtente context,GraphHopper h){
        gui = context;
        hopper=h;
    }
    @Override
    protected String doInBackground(Object... params) {
        flag=(int)params[0];
        switch(flag) {
            //carico il grafo in memoria
            case 0:
                graph();
                break;
            //calcolo un percorso usando DIJKSTRA
            case 1:
                double lat = (double) params[1];
                double lon = (double) params[2];
                double[][] raccolta = (double[][]) params[3];
                resp = new GHResponse[raccolta.length];
                //calcolo tutti percorsi possibili per ogni punto di raccolta
                for (int i = 0; i < raccolta.length; i++) {
                    calcPath(lat, lon, raccolta[i][0], raccolta[i][1]);
                }
                //scelgo il percorso che ha peso minore
                minPath = null;
                for (int i = 0; i < raccolta.length; i++) {
                    if (!resp[i].hasErrors()) {
                        Log.i("FinalPATH", "PATH n." + i + ": " + resp[i].getRouteWeight());
                        if (i == 0) {
                            minPath = resp[i];
                            nMinPath = 0;
                            continue;
                        }
                        if (minPath.getRouteWeight() > resp[i].getRouteWeight()) {
                            minPath = resp[i];
                            nMinPath = i;
                            continue;
                        }
                    }else   Log.i("FinalPATH", "PATH n." + i + " ha dato errori!");
                }
                Log.i("FinalPATH", "Ho scelto il path n." + nMinPath);
        }
        return null;
    }

    protected void onPostExecute(String result){
        switch(flag){
            case 0:
                gui.graphLoaded=true;
                gui.appClient.saveGraph(hopper);
                gui.getRaccolta();
                //stampainfo();
                break;
            case 1:
                stampa(minPath,nMinPath);
                gui.dismissPD();

                boolean locOK=gui.appClient.locationManager.checkGeo();
                if((!Setup.testMode && !locOK) ||
                        (Setup.testMode && !Setup.gpsFakeMode) ||
                        (Setup.testMode && Setup.gpsFakeMode && !locOK))
                    gui.getMappingEdge(nextEdge());

                break;
        }
    }

    //calcPath(start.latitude, start.longitude, end.latitude, end.longitude);
    public void calcPath( final double fromLat, final double fromLon, final double toLat, final double toLon ){
        StopWatch sw = new StopWatch().start();
        req = new GHRequest(fromLat, fromLon, toLat, toLon).setAlgorithm(AlgorithmOptions.DIJKSTRA_BI);
        req.getHints().put("instructions", "false");
        resp[nresp++] = hopper.route(req);
        time = sw.stop().getSeconds();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    protected void stampa(GHResponse response,int num) {
        gui.myWebView.loadUrl("javascript:setNrac("+num+")");

        double[] punto;
        PointList tmp = response.getPoints();
        punti=new double[response.getPoints().getSize()][];
        for (int i = 0; i < response.getPoints().getSize(); i++){
            punto=new double[2];
            punto[0]=tmp.getLatitude(i);
            punto[1]=tmp.getLongitude(i);
            punti[i]=punto;
        }
        //for (int i = 0; i < punti.length; i++){ Log.i("PUNTI", i + ") lat=" + punti[i][0] + " lon=" + punti[i][1]);        }

        try {
            JSONArray j=new JSONArray(punti);
            JSONObject jo=new JSONObject();
            jo.put("punti", j);
            gui.wai.setPunti(jo.toString());
            gui.myWebView.loadUrl("javascript:getPunti()");
            //decido se filtrare i punti oppure no: li filtro solo se il GPS non funziona bene o non e' stato attivato
            boolean locOK=gui.appClient.locationManager.checkGeo();
            if((!Setup.testMode && !locOK) ||
                    (Setup.testMode && !Setup.gpsFakeMode) ||
                    (Setup.testMode && Setup.gpsFakeMode && !locOK))
            filtra(punti);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }




    public void graph() {
        Log.i("INFO", "graph()...");
        MyGraphHopper tmpHopp = new MyGraphHopper();
        tmpHopp.forMobile();
        tmpHopp.setCHEnable(false);
        tmpHopp.load(new File(Setup.mapFolder).getAbsolutePath());
        Log.i("GRAPH","found graph " + tmpHopp.getGraph().toString() + ", nodes:" + tmpHopp.getGraph().getNodes());
        hopper = tmpHopp;
    }

    protected class MyGraphHopper extends GraphHopper {

        public MyGraphHopper(){      super();        }

        @Override
        public Weighting createWeighting( WeightingMap wMap, FlagEncoder encoder ){
            return new CustomWeight(encoder,gui);
        }
        /*
        public List<Integer> routePaths(double startY, double startX, double endY, double endX){

            //Examine a route and return edgeIDs that GraphHopper uses
            LocationIndex index = this.getLocationIndex();
            GHRequest request = new GHRequest(startY, startX, endY, endX);
            GHResponse response = new GHResponse();
            List<com.graphhopper.routing.Path> paths = getPaths(request, response);
            List<Integer> edges = new ArrayList<Integer>();
            for(com.graphhopper.routing.Path p:paths){
                for(EdgeIteratorState e:p.calcEdges()){
                    edges.add(e.getEdge());
                }
            }
            if (response.hasErrors()) return null;

            //Get edges for start and end point as well
            QueryResult qr = index.findClosest(startY, startX, EdgeFilter.ALL_EDGES );
            edges.add(qr.getClosestEdge().getEdge());
            qr = index.findClosest(endY, endX, EdgeFilter.ALL_EDGES );
            edges.add(qr.getClosestEdge().getEdge());

            return edges;
        }
        */
    }


    private void stampainfo(){
        //Log.i("INFO","toString="+hopper.getGraph().toString());
        //Log.i("INFO", "toDetailString=" + hopper.getGraph().toDetailsString());
        GraphStorage gh=hopper.getGraph();
        AllEdgesIterator edges=gh.getAllEdges();
        Log.i("INFO", "n.edges=" + edges.getCount());

        String stringa="";
        String s;
        int j=0;
        while(edges.next()){
            //Log.i("INFO", edges.toString() + " | distance=" + edges.getDistance());
            int A=edges.getBaseNode();
            int B=edges.getAdjNode();
            s="edges["+j+"]=new Array(new Array("+gh.getNodeAccess().getLat(A)+","+gh.getNodeAccess().getLon(A)+"),new Array("+gh.getNodeAccess().getLat(B)+","+gh.getNodeAccess().getLon(B)+"));";
            Log.i("EDGES",s);
            j++;
            stringa=stringa+s+"\n";
        }
        Log.i("INFO","n.nodes="+gh.getNodes());
        //for(int i=0;i<gh.getNodes();i++)    Log.i("INFO",i+") lat="+gh.getNodeAccess().getLat(i)+" | lon="+gh.getNodeAccess().getLon(i));
        //generaFile(stringa);
    }

    /*
    public void generaFile(String s){
        try{
            File file = new File(Setup.appFolder, "prova.txt");
            FileWriter writer = new FileWriter(file);
            writer.append(s);
            writer.flush();
            writer.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }   */


    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void filtra(double[][] punti){
        ris=new ArrayList<ArrayList<Double>>();
        int m=0;
        GraphStorage gh=hopper.getGraph();
        //filtro i punti
        for(int i=0;i<punti.length;i++){
            double lat=punti[i][0];
            double lon=punti[i][1];
            for(int j=0;j<gh.getNodes();j++){
                double latgh=gh.getNodeAccess().getLat(j);
                double longh=gh.getNodeAccess().getLon(j);
                if(lat==latgh && lon==longh){
                    ris.add(new ArrayList<Double>());
                    ris.get(m).add(lat);
                    ris.get(m).add(lon);
                    m++;
                }
            }
        }

        //creo il JSON dei punti da passare a openlayers
        //for (int i = 0; i < ris.size(); i++){   Log.i("RIS", i + ") lat=" + ris.get(i).get(0) + " lon=" + ris.get(i).get(1));        }

        try {
            JSONArray j=new JSONArray(ris);
            JSONObject jo=new JSONObject();
            jo.put("punti", j);
            gui.wai.setPunti2(jo.toString());
            gui.myWebView.loadUrl("javascript:getPunti2()");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public int nextEdge(){
        LocationIndex index=hopper.getLocationIndex();
        QueryResult qr=null;
        EdgeIteratorState edge=null;
        double difflat,difflon;
        double delta=0.000001;

        //controllo se ci sono punti filtrati
        if(ris.size()>0 && punti.length>1){
            //se il primo punto creato e' un punto filtrato, allora cerco l'edge tra questo punto e il successivo
            if(punti[0][0]==ris.get(0).get(0) && punti[0][1]==ris.get(0).get(1)){
                difflat=(punti[1][0]-punti[0][0])/2;
                difflon=(punti[1][1]-punti[0][1])/2;
                qr = index.findClosest(punti[0][0] + difflat, punti[0][1] + difflon, EdgeFilter.ALL_EDGES);
                edge=qr.getClosestEdge();
                return edge.getEdge();
            }

            difflat=Math.abs(punti[0][0]-punti[1][0]);
            difflon=Math.abs(punti[0][1]-punti[1][1]);
            //il primo punto e' lontano dal secondo
            if(difflat>delta || difflon>delta) {
                qr = index.findClosest(punti[0][0], punti[0][1], EdgeFilter.ALL_EDGES);
                edge=qr.getClosestEdge();
                return edge.getEdge();
            }
            //il primo punto e' sopra al secondo
            if(punti.length>2){
                difflat=(punti[2][0]-punti[1][0])/2;
                difflon=(punti[2][1]-punti[1][1])/2;
                qr = index.findClosest(punti[1][0] + difflat, punti[1][1] + difflon, EdgeFilter.ALL_EDGES);
                edge=qr.getClosestEdge();
                return edge.getEdge();
            }
            //ho solo due punti
            difflat=(punti[1][0]-punti[0][0])/2;
            difflon=(punti[1][1]-punti[0][1])/2;
            qr = index.findClosest(punti[0][0] + difflat, punti[0][1] + difflon, EdgeFilter.ALL_EDGES);
            edge=qr.getClosestEdge();
            return edge.getEdge();
        }
        return -1;
    }
    public int localEdge(double lat, double lon){
        LocationIndex index=hopper.getLocationIndex();
        QueryResult q=null;
        EdgeIteratorState edge=null;


        q = index.findClosest(lat, lon, EdgeFilter.ALL_EDGES);
        edge = q.getClosestEdge();

        double distance=q.getQueryDistance();
        Log.i("Debug", "queryDistance=" + distance);
        //la distanza dall'edge deve essere minore di 15 metri
        if(distance<15) return edge.getEdge();
        else return -1;
    }
}
