package com.sheva87.prova;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;





public class SetServer extends DialogFragment {

    EditText url;

    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.alert_server, null);

        url=(EditText)v.findViewById(R.id.label_url);
        //url.setText(Setup.urlServer,null);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout


        builder.setView(v)
                // Add action buttons

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {


                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...




                        String s = url.getText().toString();
                        Setup.urlServer = "http://" + s + ".ngrok.io/EMserver/webresources/";
                        //Setup.urlServer =s;
                        Setup.urlMap=Setup.urlServer+"map/";
                        Setup.urlPesi=Setup.urlServer+"weights/";
                        Setup.urlPost=Setup.urlServer+"device/";

                        ((GUIUtente)getActivity()).scriviServer(s);
                        ((GUIUtente)getActivity()).reload();


                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                    }
                });
        return builder.create();
    }

}
