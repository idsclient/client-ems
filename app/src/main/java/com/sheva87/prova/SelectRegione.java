package com.sheva87.prova;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

/**
 * Created by marco on 01/07/15.
 */
public class SelectRegione extends DialogFragment {
    private Context context;
    private String[] regioni;
    private int selected;

    public void init(Context c){
        context = c;
        selected = 2;

        regioni = new String[]{"Lazio","Toscana","Marche","Umbria","Piemonte","Liguria","Veneto","Trentino Alto Adige",
                                "Friuli Venezia Giulia","Lombardia","Puglia","Abruzzo","Sicilia","Calabria","Sardegna",
                                "Emilia Romagna","Molise","Campania","Valle d'Aosta"};

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Scegli la zona da scaricare:");
        // Specify the list array, the items to be selected by default (null for none),
        // and the listener through which to receive callbacks when items are selected
        builder.setSingleChoiceItems(regioni, 0, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                selected=which;
            }
        });
        // Set the action buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                ((GUIUtente)context).mapName = regioni[selected];
                ((GUIUtente)context).toastL("Mappa da scaricare: "+regioni[selected]);
                ((GUIUtente)context).riavvia();
            }
        });

        return builder.create();
    }
}
