package com.sheva87.prova;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.JavascriptInterface;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.io.UnsupportedEncodingException;

import static android.widget.Toast.*;

public class GUIUtente extends ActionBarActivity {

    public Context context=this;
    protected TextView tGPS, tlocGPS;
    protected TextView tNetwork, tlocNet;
    private Button salvami;
    private Button impostapos;
    private Button cerca;
    protected WebView myWebView;
    private LinearLayout layoutInd;
    private LinearLayout layoutGps;
    private LinearLayout layoutNet;
    protected Client appClient;
    private DBService dbms;
    WebAppInterface wai;
    private ProgressDialog pd;

    boolean chosed=false;
    boolean test=false;
    boolean gotMap=false;
    public String mapName=null;
    private double[][] raccolta=null;
    public boolean graphLoaded=false;
    public boolean useLOS=false;
    private boolean is_running=false;
    protected double[] geocodePos;
    public boolean flagPrimoAvvio=false;
    public boolean salvamiOn=false;
    public boolean posAttuale=false;
    public int modalita=0;
    public boolean locStarted=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);

        //recupero tutti gli elementi della view
        myWebView = (WebView) findViewById(R.id.webview);
        tGPS = (TextView) findViewById(R.id.gps);
        tlocGPS = (TextView) findViewById(R.id.locgps);
        tNetwork = (TextView) findViewById(R.id.network);
        tlocNet = (TextView) findViewById(R.id.locnet);
        salvami = (Button) findViewById(R.id.salvami);
        impostapos = (Button) findViewById(R.id.impostapos);
        layoutInd = (LinearLayout) findViewById(R.id.layoutInd);
        layoutGps = (LinearLayout) findViewById(R.id.layoutgps);
        layoutNet = (LinearLayout) findViewById(R.id.layoutnet);
        cerca = (Button) findViewById(R.id.buttonCerca);

        //impostazioni WEBVIEW
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccessFromFileURLs(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {myWebView.setWebContentsDebuggingEnabled(true);}
        myWebView.loadUrl(Setup.canvasRenderer);

        myWebView.setWebViewClient(new WebViewCanvas());
        wai=new WebAppInterface(this);
        myWebView.addJavascriptInterface(wai, "Android");

        appClient = new Client(this);
        bottoni();
        //leggo l'indirizzo MAC dello smartphone
        Setup.MAC=appClient.findMAC();

    }
    @Override
    protected void onResume() {
        super.onResume();
        if(locStarted){
            if(!appClient.locationManager.started){
                appClient.locationManager.start();
                appClient.locationManager.icone();
            }
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        appClient.locationManager.stop();
    }
    @Override
    protected void onStop() {
        super.onStop();
        appClient.locationManager.stop();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        appClient.locationManager.stop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_guiutente, menu);
        return true;
    }

    //imposto le selezioni del menu'
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         int id = item.getItemId();
         if (id == R.id.test_mode){
             Setup.testMode = ! Setup.testMode;
             chosed=false;
             resetPercorso();
             Setup.gpsFakeMode=false;
             impostapos.setText("Imposta Posizione");
             salvami.setText("Salvami!");
             is_running=false;
             salvamiOn=false;
             appClient.resetLoc();
             appClient.locationManager.resetLoc();
             Log.i("Ingo :", "Test Mode = " + Setup.testMode);
             //attivo Test mode
             if (Setup.testMode) {
                 layoutGps.setVisibility(LinearLayout.VISIBLE);
                 layoutNet.setVisibility(LinearLayout.VISIBLE);
                 layoutInd.setVisibility(LinearLayout.GONE);
                 item.setTitle("GPS Mode");
                 makeText(this.context, "Sei in Test Mode", LENGTH_LONG).show();
                 myWebView.loadUrl("javascript:initPos(" + Setup.lon + ", " + Setup.lat + ")");
             //attivo GPS mode
             } else{
                 layoutGps.setVisibility(LinearLayout.GONE);
                 layoutNet.setVisibility(LinearLayout.GONE);
                 layoutInd.setVisibility(LinearLayout.GONE);
                 item.setTitle("Test Mode");
                 makeText(this.context, "Uscito dal Test Mode", LENGTH_LONG).show();
                 appClient.locationManager.checkGeolocation();
             }

             return true;

         }
        //opzione del menu' per cambiare l'indirizzo del server
        if (id == R.id.set_server) {
            SetServer pippo = new SetServer();
            pippo.show(getSupportFragmentManager(),"SetServer");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Setto i listener dei bottoni
    public void bottoni(){
        //Listener per il bottone "Imposta Posizione"
        impostapos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                String s = b.getText().toString();

                //click su Imposta Posizione
                if (s.equals("Imposta Posizione")) {
                    layoutGps.setVisibility(LinearLayout.GONE);
                    layoutNet.setVisibility(LinearLayout.GONE);
                    layoutInd.setVisibility(LinearLayout.VISIBLE);
                    if(chosed)  switchChosed();
                    b.setText("Annulla");
                    closeKeyboard(impostapos);
                    myWebView.loadUrl("javascript:setFlag(true)");
                    if (is_running) {
                        myWebView.loadUrl("javascript:stopSimulation()");
                    }
                    is_running = false;
                    alert("Imposta la tua posizione usando il TAP, puoi aiutarti inserendo l'indirizzo");
                //click su Annulla
                } else if (s.equals("Annulla")) {
                    //resetPercorso();
                    layoutGps.setVisibility(LinearLayout.VISIBLE);
                    layoutNet.setVisibility(LinearLayout.VISIBLE);
                    layoutInd.setVisibility(LinearLayout.GONE);
                    b.setText("Imposta Posizione");

                    /*
                    if (chosed) switchChosed();

                    myWebView.loadUrl("javascript:stopSimulation()");
                    myWebView.loadUrl("javascript:setFlag(false)");
                    is_running = false;
                    */
                }
            }
        });
        //listener  per il bottone "Salvami!"
        salvami.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                String s = b.getText().toString();
                //click su "Salvami"
                if (s.equals("Salvami!")) {
                    if (appClient.lat == 0) {
                        makeText(context, "Per favore impostare una posizione o attivare la modalità GPS", LENGTH_LONG).show();
                    } else {
                        closeKeyboard(salvami);
                        b.setText("Stop");
                        //Log.i("DEBUG", "testMode=" + Setup.testMode + " gpsFakeMode=" + Setup.gpsFakeMode);
                        if (impostapos.getText().equals("Annulla")) {
                            impostapos.setText("Imposta Posizione");
                            myWebView.loadUrl("javascript:setFlag(false)");
                        }
                        if (Setup.testMode && !Setup.gpsFakeMode) {
                            is_running = true;
                            myWebView.loadUrl("javascript:startSimulation()");
                            //wai.getTAPPosition(appClient.lon, appClient.lat);
                            percorso();
                        }
                        if (Setup.testMode && Setup.gpsFakeMode) {
                            salvamiOn = true;
                            resetPercorso();
                            percorso();
                        }
                        if (!Setup.testMode) {
                            salvamiOn = true;
                            resetPercorso();
                            percorso();
                        }
                    }
                    //click su "Stop"
                } else if (s.equals("Stop")) {
                    b.setText("Salvami!");
                    is_running = false;
                    salvamiOn = false;
                    //if(chosed)  switchChosed();
                    myWebView.loadUrl("javascript:stopSimulation()");
                }
            }
        });
        //listener  per il bottone Cerca
        cerca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard(cerca);
                resetPercorso();
                setPosByAddress();
            }
        });
    }



    //Interfaccia Android - JS OpenLayers
    class WebAppInterface {
        Context mContext;
        String punti=null;  //stringa JSON dei punti del percorso calcolato
        String punti2=null; //stringa JSON dei punti filtrati del percorso calcolato

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void showToastN(int n){
            Log.i("INFO", "n=" + n);
        }
        @JavascriptInterface
        public void showToastD(double d){
            Log.i("INFO", "d="+d);
        }
        @JavascriptInterface
        public void showToast(String s){
            Log.i("INFO", "s=" + s);
            //Toast.makeText(mContext,s,Toast.LENGTH_LONG);
            //alert(s);
        }
        @JavascriptInterface
        public void showLog(String s){
            Log.i("INFO", "s=" + s);
        }
        //funzioni per settare i punti del percorso sulla mappa
        public void setPunti(String p){
            punti=p;
        }
        public void setPunti2(String p){
            punti2=p;
        }
        @JavascriptInterface
        public String getPunti(){
            return punti;
        }
        @JavascriptInterface
        public String getPunti2(){
            return punti2;
        }
        @JavascriptInterface
        public void swChosed() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    salvami.setText("Salvami!");
                    is_running = false;
                    myWebView.loadUrl("javascript:stopSimulation()");
                    toastL("Sei arrivato!!");
                    switchChosed();
                }
            });


        }
        //funzione per prendere la posizione tramite TAP
        @JavascriptInterface
        public void getTAPPosition(double lonn, double latt){
            appClient.setPos(latt, lonn);
            if(!chosed){
                if(Setup.testMode) {
                    SceltaTestMode sceltaFragment = new SceltaTestMode(latt, lonn, context);
                    sceltaFragment.show(getSupportFragmentManager(), "Tipo test");
                }else{
                    percorso();
                }
            }else {
                percorso();
            }

        }

    }

    //Init WebView dove renderizzare la Mappa
    private class WebViewCanvas extends WebViewClient {
        //funzione richiamata quando la webView finisce di caricare la pagina html contentente la mappa
        @Override
        public void onPageFinished (WebView view, String url){
            appClient.creaGPS();
            dbms = new DBService(context);
            dbms.execute(0);
        }
    }

    public void initCanvas(Object[] obj){
        if(obj!=null){
            gotMap=true;
            mapName=(String)obj[0];
            int testt=(int)obj[1];
            if(testt==1)    test=true;
            else    test=false;
            Log.i("INFO","mapName="+mapName+" test="+test);
        }

        if (gotMap){
            //non è il primo avvio, allora recupero i dati della mappa e della posizione tramite GPS
            //posizionamento();
            loadGraph();
        }else{
            //primo avvio
            PrimoAvvio fragment = new PrimoAvvio();
            fragment.init(this);
            fragment.show(getSupportFragmentManager(),"PrimoAvvio");
        }
    }
    //ricarico l'alertBox per scegliere quale mappa scaricare
    public void reload(){
        PrimoAvvio fragment = new PrimoAvvio();
        fragment.init(this);
        fragment.show(getSupportFragmentManager(), "PrimoAvvio");
    }

    //funzione che implementa l'opzione del menu "Posizione Attuale"
    public void posAttuale(){

        Log.i("INFO", "posAttuale()...");
        //1)recupero la mia posizione
        posAttuale=true;
        appClient.locationManager.checkGeolocation();
        appClient.locationManager.start();
        locStarted=true;

    }
    public void scaricaTiles(double lat, double lon){
        creaProgressDialog();
        pd.setMessage("Scaricamento nuove tiles...");
        Tiles t=new Tiles(this);
        t.scaricaTiles(lat, lon, 18);
        myWebView.loadUrl("javascript:setTilesFolder('" + Setup.tilesFolder + "')");
        myWebView.loadUrl("javascript:initPos(" + lon + ", " + lat + ")");
    }

    //al primo avvio e' stato selezionata l'opzione "TEST", allora scarico la mappa
    public void downloadMap(String chosedMap){
        creaProgressDialog();
        flagPrimoAvvio=true;
        if(chosedMap.equals(Setup.mapTest)){
            test=true;
            mapName=Setup.mapTest;
        }else{
            test=false;
            mapName=chosedMap;
        }
        pd.setMessage("1/7 : Scaricamento mappa dal server");
        appClient.getMap(mapName);


    }
    //salvo mappa e dati statici in memoria
    public void saveMap(){
        pd.setMessage("3/7 : Salvataggio dati statici nel DB locale");
        //popolo il DB locale con: pesiStatici, mapping, tipo di mappa scaricata, punti raccolta
        if(test){
            dbms= new DBService(this);
            dbms.execute(1, Setup.mapTest, 1);
        }else {
            dbms.execute(1, mapName, 0);
        }

    }
    //salvo i pesi dinamici in memoria
    public void savePesiDin(String s){
        if(flagPrimoAvvio) pd.setMessage("5/7 : Salvataggio pesi dinamici nel DB locale");
        dbms=new DBService(this);
        dbms.execute(2, s);
    }
    //carica grafo poi faccio radRaccolta e poi visRaccolta
    public void loadGraph(){
        if(!appClient.locationManager.started){
            appClient.locationManager.start();
            locStarted=true;
        }
        if(Setup.testMode) {
            //mi posiziono a cvt
            //appClient.setPos(Setup.lat,Setup.lon);
            myWebView.loadUrl("javascript:setTilesFolder('" + Setup.tilesFolder + "')");
            myWebView.loadUrl("javascript:initPos(" + Setup.lon + ", " + Setup.lat + ")");
        }else{
            //aggiornamento automatico delle coordinate con gps
            myWebView.loadUrl("javascript:setTilesFolder('" + Setup.tilesFolder + "')");
        }



        //carico i dati graphhopper
        if (flagPrimoAvvio)  pd.setMessage("6/7 : Carico i dati di Graphhopper in memoria");
        else{
            creaProgressDialog();
            pd.setMessage("1/2 : Carico i dati di Graphhopper in memoria");
        }
        appClient.loadGraph();
    }
    //calcolo il percorso dopo aver fatto getPesiDin da percorso()
    public void calcPath(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pd.dismiss();
                creaProgressDialog();
                pd.setMessage("Calcolo del percorso...\n" +
                        "2/2 Calcolo il percorso ottimo usando Dijkstra!");
            }
        });
        appClient.finalPath(appClient.lat, appClient.lon, raccolta);
    }
    /*
    //funzione che la prima volta carica i dati graphhopper in memoria, mentre le altre volte calcola il percorso ottimo
    public void posizionamento(){
        Log.i("INFO", "posizionamento()...");

        if(!appClient.locationManager.started)  appClient.locationManager.start();
        if(!graphLoaded) {
            if(Setup.testMode) {
                //mi posiziono a cvt
                //appClient.setPos(Setup.lat,Setup.lon);
                myWebView.loadUrl("javascript:setTilesFolder('" + Setup.tilesFolder + "')");
                myWebView.loadUrl("javascript:initPos(" + Setup.lon + ", " + Setup.lat + ")");
            }else{
                //aggiornamento automatico delle coordinate con gps
                myWebView.loadUrl("javascript:setTilesFolder('" + Setup.tilesFolder + "')");
            }



            //carico i dati graphhopper
            if (flagPrimoAvvio)  pd.setMessage("6/7 : Carico i dati di Graphhopper in memoria");
            else{
                creaProgressDialog();
                pd.setMessage("1/2 : Carico i dati di Graphhopper in memoria");
            }
            appClient.loadGraph();

        } else{
            //calcolo il percorso
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pd.dismiss();
                    creaProgressDialog();
                    pd.setMessage("Calcolo del percorso...\n" +
                            "2/2 Calcolo il percorso ottimo usando Dijkstra!");
                }
            });
            appClient.finalPath(appClient.lat, appClient.lon, raccolta);
        }
    }   */
    //leggo i punti di raccolta dal db locale
    public void getRaccolta(){
        //leggo i punti di raccolta
        if(flagPrimoAvvio)  pd.setMessage("7/7 : Carico i punti di raccolta sulla mappa");
        else                pd.setMessage("2/2 : Carico i punti di raccolta sulla mappa");
        dbms=new DBService(this);
        dbms.execute(3);
    }
    //funzione dell'opzione del menu iniziale "Seleziona Regione"
    public void selezionaRegione(){
        SelectRegione select = new SelectRegione();
        select.init(this);
        select.show(getSupportFragmentManager(), "SelectRegione");
    }
    //creo un alertBox
    public void alert(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setCancelable(true);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface builder, int id) {
                        builder.cancel();
                    }
                });
        AlertDialog box= builder.create();
        box.show();
    }
    //funzioni per gestire una barra di caricamento
    public void creaProgressDialog(){
        pd=new ProgressDialog(this,ProgressDialog.STYLE_SPINNER);
        pd.getWindow().setGravity(Gravity.TOP);
        pd.show();
    }
    public void setPDMessage(String message) {
        pd.setMessage(message);
    }
    public void dismissPD(){
        try {
            pd.dismiss();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    //visualizzo i punti di raccolta sulla mappa
    public void visRaccolta(double[][] rac){
        flagPrimoAvvio=false;
        raccolta=rac;
        if(raccolta==null){
            alert("Non ci sono punti di raccolta nella mappa scaricata!!");
        }else{

            for(int i=0;i<raccolta.length;i++){
                //Log.i("RACCOLTA", "raccolta[" + i + "]=" + raccolta[i][0] + "|" + raccolta[i][1]);
                myWebView.loadUrl("javascript:addPuntoRaccolta("+raccolta[i][0]+","+raccolta[i][1]+")");
            }
            myWebView.loadUrl("javascript:visRaccolta()");
        }
        dismissPD();
    }

    //scarico i pesi dinamici in memoria
    public void percorso() {
        if (graphLoaded) {
            creaProgressDialog();
            pd.setMessage("Calcolo del percorso...\n" +
                    "1/2 Recupero i pesi dinamici dal server");
            appClient.getPesiDin();
        }
        else                       alert("Il grafo non è stato caricato in memoria!");

    }
    public void getMappingEdge(int nextEdge) {
        Log.i("Edge",nextEdge+"");
        dbms = new DBService(this);
        dbms.execute(4, nextEdge);
    }

    public String getServer(){
        return dbms.readServer();
    }

    public void scriviServer(String s){
        dbms.delServer();
        dbms.writeServer(s);
    }
    public void switchChosed()
    {chosed=!chosed;}

    public void resetPercorso(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myWebView.loadUrl("javascript:resetPath()");
            }
        });

    }

    public DataWay getway(int idEdge){
       return dbms.readWay(idEdge);

    }

    public void setCursor(double latt, double lonn){
        final double lat=latt,lon=lonn;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                myWebView.loadUrl("javascript:setCursorPosition(" + lon + ", " + lat + ")");
            }
    });

    }
    public void toast(String s){
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
    }
    public void toastL(String s){
        Toast.makeText(context, s, Toast.LENGTH_LONG).show();
    }

    public void setPosByAddress(){
        EditText indirizzoLabel = (EditText) findViewById(R.id.indirizzo);
        String addr = indirizzoLabel.getText().toString();

        //closeKeyboard(cerca);

        try {
            String query = java.net.URLEncoder.encode(mapName+", "+addr, "UTF-8");
            Log.i("query: ",query);
            appClient.geocoding("http://nominatim.openstreetmap.org/search.php?q="+query+"&format=json");
            //wai.getTAPPosition(geocodePos[0],geocodePos[1]);
            // Log.i("coordinate",geocodePos[0]+" "+geocodePos[1]);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    public void closeKeyboard(View v){
        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
    public void hideImpostaPos() {
        resetPercorso();
        layoutGps.setVisibility(LinearLayout.VISIBLE);
        layoutNet.setVisibility(LinearLayout.VISIBLE);
        layoutInd.setVisibility(LinearLayout.GONE);
        impostapos.setText("Imposta Posizione");
        myWebView.loadUrl("javascript:setFlag(false)");
        //if (chosed) {            switchChosed();        }
        is_running = false;
    }
    //riavvio l'alertBox iniziale
    public void riavvia(){
        PrimoAvvio fragment = new PrimoAvvio();
        fragment.init(this);
        fragment.show(getSupportFragmentManager(),"PrimoAvvio");
    }
    public void switchModalita(int mod){

        switch(mod){
            case 1: //GPS MODE

                break;
            case 2: //TEST MODE - SIMULATION

                break;
            case 3: //TEST MODE - fakeGPS

                break;
            case 4:

                break;
        }
        modalita=mod;
    }
}
