package com.sheva87.prova;

import android.content.Context;

import com.graphhopper.routing.util.FlagEncoder;
import com.graphhopper.routing.util.Weighting;
import com.graphhopper.util.EdgeIteratorState;

class CustomWeight implements Weighting{
    private final FlagEncoder encoder;
    private final double maxSpeed;
    private DBService db;
    private DataWay way = new DataWay();
    private double[] staticloss = new double[4];
    private double k;
    private Context context;

    public CustomWeight( FlagEncoder encoder, Context c)    {
        this.encoder = encoder;
        this.maxSpeed = encoder.getMaxSpeed();
        db = new DBService(c);
        context=c;
    }

    @Override
    public double getMinWeight( double distance )    {
        return distance / maxSpeed;
    }


    @Override
    public double calcWeight( EdgeIteratorState edgeState, boolean reverse, int prevOrNextEdgeId ){

        int edge = edgeState.getEdge();
        //Log.i("INFOEdge", edgeState.toString());
        //Log.i("INFOEdge GETEDGE", edge + ") BaseNode=" + edgeState.getBaseNode() + " | AdjNode=" + edgeState.getAdjNode());

        way = db.readWay(edge);
        staticloss = db.readPesi();

        if (way!=null) {
            //Log.i("K= ", way.v+"*"+staticloss[0]+" + "+way.i+"*"+staticloss[1]+" + "+way.los+"*"+staticloss[2]+" + "+way.c+"*"+staticloss[3]);
            //Log.i("CURRENT EDGE", String.valueOf(way.idEDGE));
            double LOS=0;
            if(((GUIUtente)context).useLOS) LOS=way.los * staticloss[2];
            else   LOS=0;
            //Log.i("LOS","LOS="+LOS);
            k = (way.v * staticloss[0]) +
                                (way.i * staticloss[1]) +
                                (LOS) +
                                (way.c * staticloss[3]);
                        //Log.i("K", String.valueOf(k));
            return k;
        } else {
            //way non mappata
            k = (Setup.V * staticloss[0]) +
                    (Setup.I * staticloss[1]) +
                    (Setup.C * staticloss[3]);
            return k;
        }

    }

    @Override
    public String toString() {
        return "custom";
    }
}

