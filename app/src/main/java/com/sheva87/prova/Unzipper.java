package com.sheva87.prova;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import android.util.Log;
/**
 * Created by macbookpro on 29/06/15.
 */
public class Unzipper
{
    final int BUFFER =1024;
    String INPUT_ZIP_FILE ;
    String OUTPUT_FOLDER ;

    public Unzipper (String zipFile, String outputFolder){
        INPUT_ZIP_FILE=zipFile;
        OUTPUT_FOLDER=outputFolder;
        Log.i("zipFile:",zipFile);
        Log.i("folder:",outputFolder);
    }

    public  void unzip(){
        try{
            File destDir = new File (OUTPUT_FOLDER);
            if (!destDir.exists())  destDir.mkdir();

            ZipFile zipFile = new ZipFile(INPUT_ZIP_FILE);
            Enumeration<?> enu = zipFile.entries();
            while (enu.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) enu.nextElement();

                String name = zipEntry.getName();
                long size = zipEntry.getSize();
                long compressedSize = zipEntry.getCompressedSize();
                Log.i("INFO","name:" +name+" | size: " +size +" | compressed size:" +compressedSize +"\n");

                // Do we need to create a directory ?
                File file = new File(destDir.toString()+File.separator+name);
                if (name.endsWith("/")) {
                    file.mkdirs();
                    continue;
                }

                File parent = file.getParentFile();
                if (parent != null) {
                    parent.mkdirs();
                }

                // Extract the file
                InputStream is = zipFile.getInputStream(zipEntry);

                FileOutputStream fos = new FileOutputStream(file);

                BufferedOutputStream dest = new BufferedOutputStream (fos,BUFFER);
                byte[] bytes = new byte[1024];
                int length;
                while ((length = is.read(bytes)) >= 0) {
                    dest.write(bytes, 0, length);
                }
                is.close();
                dest.close();
                fos.close();
            }
            zipFile.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
