package com.sheva87.prova;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

public class GPS {
    private GUIUtente gui;
    private LocationManager locationManager;

    private double lat, lon;
    private float accuracy;
    private long time=-1;
    private static long deltaT=10*60000; //10 min e' la scadenza per prendere una nuova location, a prescindere dall'accuratezza
    private static float deltaA=20;     //20 metri di accuratezza
    private LocationListener llGPS=null, llNetwork=null;
    private double [] diff= new double[2];
    private double plat=0,plon=0;
    private boolean diffisset= false;
    private int idEdge;
    public boolean started=false;
    public boolean gpsOK=false;
    public boolean netOK=false;

    public GPS(GUIUtente g){
        gui=g;
        icone();
    }
    public void icone(){
        locationManager = (LocationManager) gui.getSystemService(Context.LOCATION_SERVICE);
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            Log.i("INFO", "GPSProviderEnabled");
            gui.tGPS.setText("GPS ON!");
            gui.myWebView.loadUrl("javascript:GPSOn()");
            gpsOK=true;
        }else{
            Log.i("INFO", "GPSProviderDisabled");
            gui.tGPS.setText("GPS OFF!");
            gui.myWebView.loadUrl("javascript:GPSOff()");
            gpsOK=false;
        }
        if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            Log.i("INFO","NetworkProviderEnabled");
            gui.tNetwork.setText("Network ON!");
            gui.myWebView.loadUrl("javascript:NetworkOn()");
            netOK=true;
        }else{
            Log.i("INFO", "NetworkProviderDisabled");
            gui.tNetwork.setText("Network OFF!");
            gui.myWebView.loadUrl("javascript:NetworkOff()");
            netOK=false;
        }
    }
    public boolean checkGeo(){
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
            return false;
        else{
            if(!gpsOK && !netOK)    return false;
            else    return true;
        }
    }
    //controllo se e' attivata la geolocalizzazione
    public void checkGeolocation(){
        //CONTROLLO GEOLOCALIZZAZIONE:
        //Se entrambi i provider sono disabilitati allora apro un alertdialog per poter aprire le impostazioni
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){

            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(gui);
            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage(R.string.geoAlertText).setTitle(R.string.geoAlertTitle);
            // Add the buttons
            builder.setPositiveButton(R.string.ignora, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                }
            });
            builder.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    gui.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            });

            // Create the AlertDialog
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    public void stop(){
        if(llGPS!=null){
            locationManager.removeUpdates(llGPS);
            llGPS=null;
        }
        if(llNetwork!=null){
            locationManager.removeUpdates(llNetwork);
            llNetwork=null;
        }
        started=false;
    }
    public void start(){
        //checkGeolocation();
        //listener GPS////////////////////////////////
            llGPS = new LocationListener() {
            public void onLocationChanged(Location loc) {
                // Called when a new location is found by the network location provider.

                final Location location=loc;
                Log.i("INFO","LocationGPS:"+ location.getLatitude()+"|"+location.getLongitude()+"|"+location.getAccuracy());
                gui.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gui.tlocGPS.setText("Loc=" + location.getLatitude() + "|" + location.getLongitude()+"|"+location.getAccuracy());

                    }
                });

                if(Setup.testMode && Setup.gpsFakeMode){
                    //se non ho fatto il tap per settare il punto iniziale da usare come Fake allora non faccio nulla
                    if(plat==0 && plon==0)  return;
                    //se non ho settato la differenza per il fake allora la setto ora
                    if(!diffisset)  setdiff(location);

                    fakeLocation(location);
                    return;
                }
                position(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                final String s;
                switch(status){
                    case LocationProvider.AVAILABLE :
                        s="AVAILABLE";
                        gpsOK=true;
                        break;
                    case LocationProvider.OUT_OF_SERVICE:
                        s="OUT_OF_SERVICE";
                        gpsOK=false;
                        break;
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        s="TEMPORARILY_UNAVAILABLE";
                        gpsOK=false;
                        break;
                    default: s="";
                }
                Log.i("INFO", "GPSStatusChanged=" + s);
                gui.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        gui.tGPS.setText("GPS ON! status=" + s);
                    }
                });
            }

            public void onProviderEnabled(String provider) {
                Log.i("INFO", "GPSProviderEnabled");
                gpsOK=true;
                gui.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gui.tGPS.setText("GPS ON!");
                        gui.myWebView.loadUrl("javascript:GPSOn()");
                    }
                });

            }

            public void onProviderDisabled(String provider) {
                Log.i("INFO", "GPSProviderDisabled");
                gpsOK=false;
                gui.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gui.tGPS.setText("GPS OFF");
                        gui.myWebView.loadUrl("javascript:GPSOff()");
                    }
                });
            }
        };

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, llGPS);

        //listener Network////////////////////////////////
        llNetwork = new LocationListener() {
            public void onLocationChanged(Location loc) {
                // Called when a new location is found by the network location provider.
                final Location location=loc;
                Log.i("INFO","LocationNetwork:"+ location.getLatitude()+"|"+location.getLongitude()+"|"+location.getAccuracy());
                gui.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gui.tlocNet.setText("Loc=" + location.getLatitude() + "|" + location.getLongitude() + "|" + location.getAccuracy());

                    }
                });
                if(Setup.testMode && Setup.gpsFakeMode){
                    //se non ho fatto il tap per settare il punto iniziale da usare come Fake allora non faccio nulla
                    if(plat==0 && plon==0)  return;
                    //se non ho settato la differenza per il fake allora la setto ora
                    if(!diffisset)  setdiff(location);

                    fakeLocation(location);
                    return;
                }
                position(location);

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                final String s;
                switch(status){
                    case LocationProvider.AVAILABLE :
                        netOK=true;
                        s="AVAILABLE";
                        break;
                    case LocationProvider.OUT_OF_SERVICE:
                        netOK=false;
                        s="OUT_OF_SERVICE";
                        break;
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        netOK=false;
                        s="TEMPORARILY_UNAVAILABLE";
                        break;
                    default: s="";
                }
                Log.i("INFO", "NetworkStatusChanged=" + s);
                gui.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gui.tNetwork.setText("Network ON! status=" + s);
                    }
                });

            }

            public void onProviderEnabled(String provider) {
                Log.i("INFO", "NetworkProviderEnabled");
                netOK=true;
                gui.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gui.tNetwork.setText("Network ON!");
                        gui.myWebView.loadUrl("javascript:NetworkOn()");
                    }
                });

            }

            public void onProviderDisabled(String provider) {
                Log.i("INFO","NetworkProviderDisabled");
                netOK=false;
                gui.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gui.tNetwork.setText("Network OFF");
                        gui.myWebView.loadUrl("javascript:NetworkOff()");
                        }
                });

            }
        };
        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, llNetwork);
        started=true;
    }

    //filtro le posizioni in base a tempo e accuratezza
    public void position(Location location){
        if(gui.posAttuale){
            gui.scaricaTiles(location.getLatitude(),location.getLongitude());
            gui.posAttuale=false;
            return;
        }

        if(!Setup.testMode || (Setup.testMode && Setup.gpsFakeMode)) {
            if ((location.getTime() - time) > deltaT || (location.getAccuracy() - accuracy) < deltaA) {
                lat = location.getLatitude();
                lon = location.getLongitude();
                accuracy = location.getAccuracy();
                time = location.getTime();
                gui.appClient.setPos(lat, lon);
                //filtro solo i punti che distano 1 metro l'uno dall'altro
                if (gui.appClient.testDist(1)) {
                    gui.setCursor(lat, lon);
                }
            }
        }
    }

    //recupero l'ultima posizione
    public double[] getPosition(){
        if(time<0) return null;
        else {
            double[] d = new double[2];
            d[0] = lat;
            d[1] = lon;
            return d;
        }
    }
    public long getTime(){
        return time;
    }
    //imposto una posizione falsa al posto di quella rilevata dal gps
    public void fakeLocation(Location location){

        lat=location.getLatitude();
        lon=location.getLongitude();
        if (diffisset){
            lat-=diff[0];
            Log.i("INFO","latitudine falsa passata: "+lat);
            lon+=diff[1];
            Log.i("INFO","longitudine falsa passata: "+lon);
            location.setLatitude(lat);
            location.setLongitude(lon);

            position(location);
        }

    }
    //setto longitudine del punto finto
    public void plonset(double p){
        plon=p;
    }
    //setto latitudine del punto finto
    public void platset(double p){
        plat=p;
    }

    //calcolo la differenza tra punto finto (a Civitanova) e punto rilevato tramite gps
    public  void  setdiff(Location location){
        diff[0] = location.getLatitude() - plat;
        diff[1] = plon-location.getLongitude();
        diffisset=true;
        Log.i("INFO","differenza tra i punti lat/lon: "+diff[0]+"/"+diff[1]);

    }
    public void resetLoc(){
        lat=0;
        lon=0;
        time=0;
        //private double [] diff= new double[2];
        plat=0;
        plon=0;
        diffisset= false;
        //private int idEdge;
    }
}

