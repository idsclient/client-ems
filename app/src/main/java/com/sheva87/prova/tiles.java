package com.sheva87.prova;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;

import java.io.File;

/**
 * Created by Sheva87 on 16/06/2015.
 */
public class Tiles {
    Context context;
    int x,y,z;
    String[] urls;
    int nurls;
    int cursorUrl;
    int[][] dirTiles;
    File folder;
    int m;
    public Tiles(Context cx){
        context=cx;
        x=0;
        y=0;
        z=0;
        cursorUrl=0;
        //nurls=13*13+7*7+4*4+2*2+1;
        nurls=1000;
        urls=new String[nurls];
        dirTiles=new int[nurls][3];
    }
    public void scaricaTiles(double lat, double lon, int zoom){
        CSI csi=new CSI((GUIUtente)context);
        if(!csi.checkConnection()){
            ((GUIUtente)context).alert("Connessione internet assente!");
            return;
        }
        calcola(lat, lon, zoom);
        calcola2km2();
        Log.i("scarica", "n. tiles= " + urls.length);

        TileTask tiletask=new TileTask(context);
        tiletask.execute(m,urls,dirTiles);
        /*
        for(int i=0;i<m;i++){
            Log.i("scarica","scarica tile n."+i);
            File f=new File(Setup.tilesFolder,"/"+dirTiles[i][0]+"/"+dirTiles[i][1]+"/"+dirTiles[i][2]+".png");
            if(f.exists()){
                Log.i("scarica", "Tile gia' esistente!! /"+dirTiles[i][0]+"/"+dirTiles[i][1]+"/"+dirTiles[i][2]+".png");
                continue;
            }
            Intent intent=new Intent(context, TileService.class);
            intent.putExtra("url",urls[i]);
            intent.putExtra("z",dirTiles[i][0]);
            intent.putExtra("x",dirTiles[i][1]);
            intent.putExtra("y",dirTiles[i][2]);
            context.startService(intent);
        }*/
    }
    //calcolo la tile principale in cui mi trovo, in base allo zoom
    public void calcola(double lat, double lon, int zoom){
        Log.i("tiles", "calcola()");
        /*
        n = 2 ^ zoom
        xtile = n * ((lon_deg + 180) / 360)
        ytile = n * (1 - (log(tan(lat_rad) + sec(lat_rad)) / ?)) / 2
        */
        z=zoom;
        double nn;
        double xx,yy;
        nn=Math.pow(2,zoom);
        xx=nn*((lon+180)/360);
        double lat_rad=lat*2*Math.PI/360;
        double sec=1/Math.cos(lat_rad);
        yy=nn*(1-(Math.log(Math.tan(lat_rad)+sec)/Math.PI))/2;
        Log.i("TILES", "n=" + nn + " | x=" + xx + " | y=" + yy);
        x=(int)xx;
        y=(int)yy;
    }

    public void calcola2km2() {
        Log.i("tiles", "calcola2km2()");
        m = 0;
        Log.i("TILES2km", "z=" + z + " | x=" + x + " | y=" + y);
        //ZOOM=18
        int xa=x-6;
        int ya=y-6;
        int xb=x+7;
        int yb=y+7;
        for(int i=xa;i<xb;i++){
            for(int j=ya;j<yb;j++){
                urls[m]="http://a.tile.openstreetmap.org/"+z+"/"+i+"/"+j+".png";
                dirTiles[m][0]=z;
                dirTiles[m][1]=i;
                dirTiles[m][2]=j;
                m++;
            }
        }

        while(z>14) {
            z--;
            xa = xa / 2 - 1;
            ya = ya / 2 - 1;
            xb = xb / 2 + 1;
            yb = yb / 2 + 1;
            for(int i=xa;i<xb;i++){
                for(int j=ya;j<yb;j++){
                    urls[m]="http://a.tile.openstreetmap.org/"+z+"/"+i+"/"+j+".png";
                    dirTiles[m][0]=z;
                    dirTiles[m][1]=i;
                    dirTiles[m][2]=j;
                    m++;
                }
            }
        }

    }

}
