package com.sheva87.prova;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.Properties;

/**
 * Created by marco on 29/06/15.
 */
public class HttpRequest {

    protected HttpURLConnection connection;
    private String USER_AGENT = "Mozilla/5.0";
    protected int responseCode;
    public String method;
    public URL url;
    public String response;
    protected InetAddress ip;
    private int id1,id2;
    public boolean geocode = false;

    public String zipfilepath;

    //Constructor
    public HttpRequest() {

    }

    //GET
    public void sendRequest(String methodd, String urlRequest) throws Exception {
        //inizializzo i parametri interni alla classe
        method = methodd;
        url = new URL(urlRequest);
        connection = (HttpURLConnection) url.openConnection();
        //setto metodo di connessione http
        connection.setRequestMethod(method);
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setConnectTimeout(5000);

        DataOutputStream wr;
        String position;
        //a seconda del metodo prescelto invio richieste diverse al server
        switch (method) {
            case "GET":
                String map = null,map_path = null;
                ///geocoding con indirizzo
                if(geocode){
                    connection.setRequestProperty("Content-type", "application/json");
                    responseCode = connection.getResponseCode();
                    Log.i("INFO HTTP", "codice di risposta da server: " + responseCode);
                    response = getInputData();
                    break;
                }
                ///richiesta generica
                map = urlRequest.split("/")[5];
                map_path ="map";
                //caso diverso dal get di una mappa
                if (!(map.equals(map_path))) {
                    connection.setRequestProperty("Content-type", "application/json");
                    responseCode = connection.getResponseCode();
                    Log.i("INFO HTTP", "codice di risposta da server: " + responseCode);
                    response = getInputData();
                }else {
                    //caso della get di una mappa
                    responseCode = connection.getResponseCode();
                    Log.i("INFO HTTP", "codice di risposta da server: " + responseCode);
                    String content= connection.getHeaderField("Content-Disposition");
                    String zipfilename= content.split("=")[1];
                    //Log.i("INFOZIP","Zipfilename:"+zipfilename);
                    zipfilepath = Setup.appFolder+"/"+zipfilename;
                    Log.i("INFOZIP","Zipfilepath:"+zipfilepath);
                    zipDownload();
                }
                break;

            /////////ie
            case "PUT":
                connection.setRequestProperty("Content-type", "application/json");
                connection.setDoOutput(true);
                ip = InetAddress.getLocalHost();

                wr = new DataOutputStream(this.connection.getOutputStream());
                position = "{\"macaddr\":\""+Setup.MAC+"\",\"idnode1\":\""+id1+"\",\"idnode2\":\""+id2+"\"}";
                //Log.i("PUT",position);
                wr.writeBytes(position);
                wr.flush();
                wr.close();

                responseCode = connection.getResponseCode();
                break;

            ///////
            case "POST":
                connection.setRequestProperty("Content-type", "application/json");
                connection.setDoOutput(true);
                ip = InetAddress.getLocalHost();

                wr = new DataOutputStream(this.connection.getOutputStream());
                position = "{\"macaddr\":\""+Setup.MAC+"\",\"idnode1\":\""+id1+"\",\"idnode2\":\""+id2+"\"}";
                Log.i("POST",position);
                wr.writeBytes(position);
                wr.flush();
                wr.close();

                responseCode = connection.getResponseCode();
                Log.i("INFO HTTP", "codice di risposta da server: " + responseCode);
                break;

            default:
                throw new Exception("Errore: il Metodo Http scelto è Errato!\n Richiesta Http Malformata!");

        }

    }

    protected String getInputData() throws IOException {
        //Init oggetti di input-stream
        BufferedReader inputStream = new BufferedReader(
                new InputStreamReader(this.connection.getInputStream()));
        String incomingData;
        StringBuffer response = new StringBuffer();

        //lettura dello stream dati in arrivo
        while ((incomingData = inputStream.readLine()) != null) {
            response.append(incomingData + "\n");
        }
        inputStream.close();

        return response.toString();
    }

    protected void zipDownload() throws Exception {
        InputStream is = this.connection.getInputStream();
        File destDir = new File (Setup.appFolder);
        if(!destDir.exists()){
            destDir.mkdir();
        }
        FileOutputStream fos = new FileOutputStream(zipfilepath);
        byte[] buffer = new byte[1024];
        int n;
        while ((n=is.read(buffer))>=0) {
            fos.write(buffer, 0, n);
        }
        fos.flush();
        fos.close();
        is.close();
    }
    public String getZipfilepath(){
        return zipfilepath;
    }
    public void getIdMapping(int id1,int id2){
        this.id1=id1;
        this.id2=id2;
    }
}
            //GET ip addr
//				client.sendRequest("GET","http://a5501589.ngrok.io/PositionsREST/webresources/service.position");
//				System.out.println("Invio richiesta "+client.method+ " a: {host->" + client.url.toString()+"}");
//				System.out.println("Risposta del server: "+client.responseCode);
//				System.out.println(client.response);
//
            //client.sendRequest("GET","http://localhost/MaterialDesign/index.html");
            //client.sendRequest("GET","http://api.openstreetmap.org/api/0.6/capabilities");
           //// client.sendRequest("GET","http://api.openstreetmap.org/api/0.6/map?bbox=16.8071,41.0795,16.9315,41.148");
            //System.out.println("Invio richiesta "+client.method+ " a: {host->" + client.url.toString()+"}");
            //System.out.println("Risposta del server: "+client.responseCode);
            //System.out.println(client.response);

