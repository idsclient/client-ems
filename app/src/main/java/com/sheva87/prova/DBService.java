package com.sheva87.prova;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import static com.sheva87.prova.DBMSClient.*;


public class DBService extends AsyncTask<Object,Void,String> {
    private DBMSClient.DbHelper mDbHelper;
    private Context context;
    private int flag=0;
    private Object[] datiMappa=null;
    private double[][] raccolta=null;
    private Cursor c;
    private DataWay way=null;

    public DBService(Context c) {
        mDbHelper = new DBMSClient.DbHelper(c);
        context=c;
    }



    @Override
    protected String doInBackground(Object... params) {
        flag=(int)params[0];


        // Do work here, based on the contents of dataString
        switch(flag){
            //Legge i pesi statici dal DB
            case 0:

                datiMappa=readMappe();
                String s=readServer();
                if(s!=null) {
                    Setup.urlServer = "http://" + s + ".ngrok.io/EMserver/webresources/";
                    Setup.urlMap = Setup.urlServer + "map/";
                    Setup.urlPesi = Setup.urlServer + "weights/";
                    Setup.urlPost = Setup.urlServer + "device/";
                }
                break;

            //popolo il DB locale con: pesiStatici, mapping, tipo di mappa scaricata, punti raccolta
            case 1:
                String mapName=(String)params[1];
                int test=(int)params[2];
                SQLiteDatabase db = mDbHelper.getWritableDatabase();
                try {
                    db.execSQL("DELETE FROM " + PesiTableContents.TABLE_NAME);
                    db.execSQL("DELETE FROM " + WayTableContents.TABLE_NAME_WAY);
                    db.execSQL("DELETE FROM " + RaccoltaTableContents.TABLE_NAME_RACCOLTA);
                    //db.execSQL("DELETE FROM " + MappeTableContents.TABLE_NAME_MAPPE);
                } catch (Exception e){
                    e.printStackTrace();
                }

                try {
                    String jsonPesiStat = jsonToString(Setup.staticJson);
                    String jsonMapping = jsonToString(Setup.mappingJson);
                    String jsonRaccolta = jsonToString(Setup.raccoltaJson);

                    jsonToStaticLoss(jsonPesiStat);
                    //readPesi();

                    jsonMapping(jsonMapping);

                    writeMappe(mapName,test);
                    jsonToRaccolta(jsonRaccolta);

                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
            //aggiorno pesiDin
            case 2:
                String JSONdin=(String)params[1];
                jsonToWay(JSONdin);
                break;
            //leggo i punti di raccolta
            case 3:
                raccolta=readRaccolta();
                break;
            //leggo il mapping dato l'id dell'edge
            case 4:
                int nextEdge=(int)params[1];
                way=readWay(nextEdge);
                break;
        }

        return null;
    }
    protected void onPostExecute(String result){
        switch(flag){
            case 0:
                ((GUIUtente)context).initCanvas(datiMappa);
                break;
            case 1:
                ((GUIUtente)context).appClient.getPesiDin();
                break;
            case 2:
                //((GUIUtente)context).posizionamento();
                if(((GUIUtente)context).graphLoaded)    ((GUIUtente)context).calcPath();
                else                                    ((GUIUtente)context).loadGraph();
                break;
            case 3:
                ((GUIUtente)context).visRaccolta(raccolta);
                break;
            case 4:
                int[] ris=null;
                if(way!=null) {
                    ris= new int[2];
                    ris[0] = way.idOSMin;
                    ris[1] = way.idOSMend;
                }
                ((GUIUtente)context).appClient.putPos(ris);
                break;
        }
    }


    public void delServer(){

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        String delServer =
                "DELETE FROM " + ServerTableContents.TABLE_NAME_SERVER;

        try {
            db.execSQL(delServer);
        } catch (Exception e){
            e.printStackTrace();
        }


    }

    public String readServer(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String s= new String();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                ServerTableContents.COLUMN_INDIRIZZO
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder ="";
        try {

            c=db.rawQuery("SELECT * FROM "+ServerTableContents.TABLE_NAME_SERVER,null);


            if (c.getCount()>0){
                c.moveToFirst();
                s=c.getString(1);
                return s;
            }

        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public void writeServer(String indirizzo){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(ServerTableContents.COLUMN_INDIRIZZO, indirizzo);
        //values.put(FeedEntry.COLUMN_NAME_CONTENT, content);

        // Insert the new row, returning the primary key value of the new row

        try {
            db.insert(
                    ServerTableContents.TABLE_NAME_SERVER,
                    null,
                    values);
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    public void writeMappe(String nome,int flag){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(MappeTableContents.COLUMN_NOME, nome);
        values.put(MappeTableContents.COLUMN_FLAG, flag);
        //values.put(FeedEntry.COLUMN_NAME_CONTENT, content);

        // Insert the new row, returning the primary key value of the new row

        try {
            db.insert(
                    MappeTableContents.TABLE_NAME_MAPPE,
                    null,
                    values);
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    public Object[] readMappe(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Object[] obj=new Object[2];

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                MappeTableContents.COLUMN_NOME,
                MappeTableContents.COLUMN_FLAG
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder ="";
        try {

            c=db.rawQuery("SELECT * FROM "+MappeTableContents.TABLE_NAME_MAPPE,null);


            if (c.getCount()>0){
                c.moveToFirst();
                obj[0]=c.getString(1);
                obj[1]=c.getInt(2);
                return obj;
            }

        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public void jsonToRaccolta(String puntiraccolta){
        JSONArray arr = null;
        try {
            arr = new JSONArray(puntiraccolta);
            for (int i=0;i<arr.length(); i++) {
                JSONObject obj = arr.getJSONObject(i);
                writeRaccolta(obj.getDouble("lat"), obj.getDouble("lon"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void writeRaccolta(double lat, double lon){
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(RaccoltaTableContents.COLUMN_LAT, lat);
        values.put(RaccoltaTableContents.COLUMN_LON, lon);
        //values.put(FeedEntry.COLUMN_NAME_CONTENT, content);

        // Insert the new row, returning the primary key value of the new row

        try {
            db.insert(
                    RaccoltaTableContents.TABLE_NAME_RACCOLTA,
                    null,
                    values);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public double[][] readRaccolta(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                RaccoltaTableContents.COLUMN_LAT,
                RaccoltaTableContents.COLUMN_LON
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder ="";
        try {
             c = db.query(
                    RaccoltaTableContents.TABLE_NAME_RACCOLTA,  // The table to query
                    projection,                               // The columns to return
                    null,                                // The columns for the WHERE clause
                    null,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
            );

            c.moveToFirst();
            int i=0;
            double[][] d = new double[c.getCount()][2];
            int var=0;
            while (c.moveToNext()) {
                if (var==0) {
                    var=1;
                    c.moveToFirst();
                }
                d[i][0]=c.getDouble(0);
                d[i][1]=c.getDouble(1);
                i++;
            }
            c.close();
            return d;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String jsonToString(String fileName){
        try {
            FileInputStream in;
            in = new FileInputStream(new File(Setup.appFolder,fileName));
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String incomingData;
            StringBuffer response = new StringBuffer();
            //lettura dello stream dati in arrivo
            while ((incomingData = reader.readLine()) != null) {
                response.append(incomingData + "\n");
            }
            in.close();

            return response.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    //****GESTIONE TABELLA PESI****
    public double[] readPesi(){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                PesiTableContents.COLUMN_pV,
                PesiTableContents.COLUMN_pI,
                PesiTableContents.COLUMN_pLOS,
                PesiTableContents.COLUMN_pC
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                PesiTableContents._ID + " DESC";
        int i=0;
        try {

             c = db.query(
                    PesiTableContents.TABLE_NAME,  // The table to query
                    projection,                               // The columns to return
                    null,                                // The columns for the WHERE clause
                    null,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
            );

            double[] d = new double[4];


            i=c.getCount();

            if (i>0) {
                c.moveToFirst();
                d[0] = c.getDouble(0);
                d[1] = c.getDouble(1);
                d[2] = c.getDouble(2);
                d[3] = c.getDouble(3);
                //Log.i("READPESI= ", d[0] + " - " + d[1] + " - " + d[2] + " - " + d[3]);

                c.close();
                return d;
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    //SCRIVI PESI STATICI SU DB
    public void writePesi(double[] pesiStatici){
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(PesiTableContents.COLUMN_pV, pesiStatici[0]);
        values.put(PesiTableContents.COLUMN_pI, pesiStatici[1]);
        values.put(PesiTableContents.COLUMN_pLOS, pesiStatici[2]);
        values.put(PesiTableContents.COLUMN_pC, pesiStatici[3]);
        //values.put(FeedEntry.COLUMN_NAME_CONTENT, content);

        // Insert the new row, returning the primary key value of the new row

        try {
            readPesi();
            db.insert(
                    PesiTableContents.TABLE_NAME,
                    null,
                    values);
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    //Dato in input la stringa in formato JSON dei pesi statici, li converte in un double[] e aggiorna la tabella StaticLoss
    public void jsonToStaticLoss(String pesi){
        int i=0;
        try {


            double[] ris = new double[4];

            JSONObject object = (JSONObject) new JSONTokener(pesi).nextValue();


            ris[0]=object.getDouble("pV");
            ris[1]=object.getDouble("pI");
            ris[2]=object.getDouble("pLOS");
            ris[3]=object.getDouble("pC");


            writePesi(ris);


        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    //Dato in input il JSON delle way, lo converte in array e lo salva nel DB locale nella tabella Ways.
    public void jsonToWay(String ways){
        JSONArray arr = null;
        double[] app= new double[4];
        try {
            arr = new JSONArray(ways);
            for (int i=0;i<arr.length();i++) {
                JSONObject obj=arr.getJSONObject(i);
                app[0]=obj.getDouble("v");
                app[1]=obj.getDouble("i");
                app[2]=obj.getDouble("los");
                app[3]=obj.getDouble("c");
                updateWayPesi(obj.getInt("idnode1"), obj.getInt("idnode2"), app);
                //Log.i("INFO NODO1=", String.valueOf(obj.getInt("idnode1")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    //scrive nella tabella WAY il mapping con tutti i pesi settati a null
    public void jsonMapping(String ways){
        JSONArray arr = null;
        try {
            arr = new JSONArray(ways);
            for (int i=0;i<arr.length();i++) {
                JSONObject obj=arr.getJSONObject(i);
                writeWay(obj.getInt("idnode1Osm"), obj.getInt("idnode2Osm"), obj.getInt("idedgeGh"), null);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void updatePesi(double[] pesi){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String s="UPDATE "+ PesiTableContents.TABLE_NAME +" SET "+
                PesiTableContents.COLUMN_pV+"="+ pesi[0]+","+
                PesiTableContents.COLUMN_pI+"="+ pesi[1]+","+
                PesiTableContents.COLUMN_pLOS+"="+ pesi[2]+","+
                PesiTableContents.COLUMN_pC+"="+ pesi[3]+
                " WHERE "+PesiTableContents._ID+"=1";

        try {
            db.execSQL(s);
        } catch (Exception e){
            e.printStackTrace();
        }
        //Log.i("INFO= ", s);
    }

    public void deletePesi() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        try {
            db.execSQL("delete from " + PesiTableContents.TABLE_NAME);
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    //****GESTIONE TABELLA WAY****

    //leggi la way in base all'edge
    public DataWay readWay(int idedge) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();


        try {

             c = db.rawQuery("SELECT * FROM " + WayTableContents.TABLE_NAME_WAY + " WHERE " + WayTableContents.COLUMN_ID_Edge + "="+idedge,null);
            c.moveToFirst();
            DataWay dataway = new DataWay();

            if (c.getCount()>0) {
                dataway.idOSMin = c.getInt(1);
                dataway.idOSMend = c.getInt(2);
                dataway.idEDGE = c.getInt(3);

                dataway.v = c.getDouble(4);
                dataway.i = c.getDouble(5);
                dataway.los = c.getDouble(6);
                dataway.c = c.getDouble(7);
                c.close();
                //Log.i("LEGGIWAY",c.getInt(0)+" - "+c.getInt(1)+" - "+c.getInt(2)+" - "+c.getInt(3)+" - "+c.getDouble(4)+" - "+c.getDouble(5)+" - "+c.getDouble(6)+" - "+c.getDouble(7));
            }else   return null;

            return dataway;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    public void writeWay(int IDin, int IDend, int IDEdge, double[] pesi){
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        //db.execSQL(DbHelper.SQL_CREATE_WAY);

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(WayTableContents.COLUMN_ID_OSMin, IDin);
        values.put(WayTableContents.COLUMN_ID_OSMend, IDend);
        values.put(WayTableContents.COLUMN_ID_Edge, IDEdge);

        //pesi dinamici
        if (pesi!=null) {
            values.put(WayTableContents.COLUMN_V, pesi[0]);
            values.put(WayTableContents.COLUMN_I, pesi[1]);
            values.put(WayTableContents.COLUMN_LOS, pesi[2]);
            values.put(WayTableContents.COLUMN_C, pesi[3]);
        }
        // Insert the new row, returning the primary key value of the new row
        try {
            db.insert(
                    WayTableContents.TABLE_NAME_WAY,
                    null,
                    values);
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public void updateWayPesi(int idIN, int idEND, double[] pesi){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String s="UPDATE "+ WayTableContents.TABLE_NAME_WAY +" SET "+
                WayTableContents.COLUMN_V+"="+ pesi[0]+","+
                WayTableContents.COLUMN_I+"="+ pesi[1]+","+
                WayTableContents.COLUMN_LOS+"="+ pesi[2]+","+
                WayTableContents.COLUMN_C+"="+ pesi[3]+
                " WHERE "+WayTableContents.COLUMN_ID_OSMin+"="+idIN+" AND "+WayTableContents.COLUMN_ID_OSMend+"="+idEND;

        try {
            db.execSQL(s);
        } catch (Exception e){
            e.printStackTrace();
        }
        //Log.i("INFO= ", s);
    }

    public void deleteWay() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        try {
            db.execSQL("delete from " + WayTableContents.TABLE_NAME_WAY);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}