package com.sheva87.prova;

import android.os.Environment;
import android.util.Log;


/**
 * Created by marco on 30/06/15.
 */
public class Setup {
    static public String appFolder=Environment.getExternalStorageDirectory()+"/graphhopper";
    static public String mapFolder=appFolder+"/gh";
    static public String tilesFolder=appFolder+"/tiles";
    static public String canvasRenderer = "file:///android_asset/mappa.html";

    static public String urlServer="http://9c3beb59.ngrok.io/EMserver/webresources/";
    static public String urlMap=urlServer+"map/";
    static public String urlPesi=urlServer+"weights/";
    static public String urlPost=urlServer+"device/";

    static public String mapTest="civitanova";
    static public String MAC;

    static public String staticJson = "static_weights.json";
    static public String mappingJson = "mapping.json";
    static public String raccoltaJson = "safepoints.json";
    static public String MACfile="MAC.txt";

    static public boolean testMode =true;
    static public boolean gpsFakeMode=false;

    //pesi per le way non mappate
    static public double V=1;
    static public double I=6.97;
    static public double C=3;

    //punti iniziali
    static public double lat = 43.30870;
    static public double lon = 13.72852;

}