package com.sheva87.prova;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

/**
 * Created by Sheva87 on 25/06/2015.
 */
public class PrimoAvvio extends DialogFragment {
    String[] scelte;
    Context context;
    int selected;
    public void init(Context c){
        context=c;
        scelte=new String[3];
        scelte[0]="Test";
        scelte[1]="Posizione attuale";
        scelte[2]="Seleziona una regione";
        selected=0;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Set the dialog title
        builder.setTitle("Scegli la zona da scaricare:");
        // Specify the list array, the items to be selected by default (null for none),
        // and the listener through which to receive callbacks when items are selected
        builder.setSingleChoiceItems(scelte, 0, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                selected=which;
            }
        });
        // Set the action buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        switch(selected){
                            case 0: //TEST
                                ((GUIUtente)context).downloadMap(Setup.mapTest);
                                break;
                            case 1: //posizione attuale
                                Setup.testMode=false;

                                ((GUIUtente)context).posAttuale();
                                //((GUIUtente)context).creaFileEdges();
                                break;
                            case 2: //regione
                                ((GUIUtente)context).selezionaRegione();
                        }
                    }
                })
                .setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(context,"ANNULLA",Toast.LENGTH_LONG).show();
                    }
                });

        return builder.create();
    }
}
