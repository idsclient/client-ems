package com.sheva87.prova;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Sheva87 on 13/07/2015.
 */
public class TileTask extends AsyncTask<Object, Void, String> {
    private Context context;
    private String folder=Setup.tilesFolder;
    public TileTask(Context c){
        context=c;
    }
    @Override
    protected String doInBackground(Object... params) {
        File fold=new File(folder);
        if(!fold.exists())  fold.mkdir();
        int m=(int)params[0];
        String[] urls=(String[])params[1];
        int[][] dirTiles=(int[][])params[2];

        for(int i=0;i<m;i++){
            Log.i("scarica", "scarica tile n." + i);
            //controllo se la tile esiste
            File f=new File(Setup.tilesFolder,"/"+dirTiles[i][0]+"/"+dirTiles[i][1]+"/"+dirTiles[i][2]+".png");
            if(f.exists()){
                Log.i("scarica", "Tile gia' esistente!! /"+dirTiles[i][0]+"/"+dirTiles[i][1]+"/"+dirTiles[i][2]+".png");
                continue;
            }
            //se la tile non esiste, allora la scarico
            String url=urls[i];
            Log.i("tiles", url);
            Bitmap b = getBitmapFromURL(url);
            if (b == null) {
                Log.i("tiles", "Bitmap NULL");
                continue;
            }
            int z=dirTiles[i][0];
            int x=dirTiles[i][1];
            int y=dirTiles[i][2];
            //cartella Z
            String pathz=z+"";
            File fz=new File(folder,pathz);
            if(!fz.exists())    fz.mkdir();
            //cartella X
            String pathx=x+"";
            File fx=new File(fz.getPath(),pathx);
            if(!fx.exists())    fx.mkdir();
            //tile Y
            String pathy=y+".png";
            File fy = new File(fx.getPath(), pathy);
            if(!fy.exists())    storeImage(b, fy);
            else    Log.i("tiles", "Tile gia' esistente!! /"+z+"/"+x+"/"+y+".png");
        }


        return null;
    }

    protected void onPostExecute(String result){
        ((GUIUtente)context).dismissPD();
        ((GUIUtente)context).toastL("Tiles scaricate al 100%");
    }


    //scarico la tile dall'url
    public Bitmap getBitmapFromURL(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Log.i("tileCode","connection.getResponseCode()"+connection.getResponseCode());
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    //salvo la tile scaricata
    private void storeImage(Bitmap image, File f) {
        File pictureFile = f;

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.d("tiles", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("tiles", "Error accessing file: " + e.getMessage());
        }
    }
}
