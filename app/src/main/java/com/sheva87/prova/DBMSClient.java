package com.sheva87.prova;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public final class DBMSClient {



        public DBMSClient() {
        }

        /* Inner class that defines the table contents */
        public static abstract class PesiTableContents implements BaseColumns {
            public static final String TABLE_NAME = "StaticLoss";
            public static final String COLUMN_pV = "pV";
            public static final String COLUMN_pI = "pI";
            public static final String COLUMN_pLOS = "pLOS";
            public static final String COLUMN_pC = "pC";
        }

        /* Inner class that defines the table contents */
        public static abstract class WayTableContents implements BaseColumns {
            public static final String TABLE_NAME_WAY = "Way";
            public static final String COLUMN_ID_OSMin = "IDin";
            public static final String COLUMN_ID_OSMend = "IDend";
            public static final String COLUMN_ID_Edge = "IDedge";

            public static final String COLUMN_V = "V";
            public static final String COLUMN_I = "I";
            public static final String COLUMN_LOS = "LOS";
            public static final String COLUMN_C = "C";
        }

        public static abstract class RaccoltaTableContents implements BaseColumns {
            public static final String TABLE_NAME_RACCOLTA = "Raccolta";
            public static final String COLUMN_LAT = "lat";
            public static final String COLUMN_LON = "lon";

        }

        public static abstract class MappeTableContents implements BaseColumns {
            public static final String TABLE_NAME_MAPPE = "Mappe";
            public static final String COLUMN_NOME = "nome";
            public static final String COLUMN_FLAG = "flag";

        }



        public static abstract class ServerTableContents implements BaseColumns {
            public static final String TABLE_NAME_SERVER = "Server";
            public static final String COLUMN_INDIRIZZO = "nome";
        }

        /* Create and maintain the database and tables */
        public static class DbHelper extends SQLiteOpenHelper {

            public static final int DATABASE_VERSION = 1;
            public static final String DATABASE_NAME = "dbclient.db";

            /* Costruttore */
            public DbHelper(Context context) {
                super(context, DATABASE_NAME, null, DATABASE_VERSION);
            }

            /* Query per la creazione del DB*/
            private static final String INT_TYPE = " INT";
            private static final String STRING_TYPE = " STRING";
            private static final String DOUBLE_TYPE = " DOUBLE";
            private static final String COMMA_SEP = ",";
            private static final String SQL_CREATE_STATICLOSS =
                    "CREATE TABLE " + PesiTableContents.TABLE_NAME + " (" +
                            PesiTableContents._ID + " INTEGER PRIMARY KEY," +
                            PesiTableContents.COLUMN_pV + DOUBLE_TYPE + COMMA_SEP +
                            PesiTableContents.COLUMN_pI + DOUBLE_TYPE + COMMA_SEP +
                            PesiTableContents.COLUMN_pLOS + DOUBLE_TYPE + COMMA_SEP +
                            PesiTableContents.COLUMN_pC + DOUBLE_TYPE +
                            ")";

            private static final String SQL_CREATE_WAY =
                    "CREATE TABLE " + WayTableContents.TABLE_NAME_WAY + " (" +
                            WayTableContents._ID + " INTEGER PRIMARY KEY," +
                            WayTableContents.COLUMN_ID_OSMin + INT_TYPE + COMMA_SEP +
                            WayTableContents.COLUMN_ID_OSMend + INT_TYPE + COMMA_SEP +
                            WayTableContents.COLUMN_ID_Edge + INT_TYPE + COMMA_SEP +

                            WayTableContents.COLUMN_V + DOUBLE_TYPE + COMMA_SEP +
                            WayTableContents.COLUMN_I + DOUBLE_TYPE + COMMA_SEP +
                            WayTableContents.COLUMN_LOS + DOUBLE_TYPE + COMMA_SEP +
                            WayTableContents.COLUMN_C + DOUBLE_TYPE +

                            ")";

            private static final String SQL_CREATE_RACCOLTA =
                    "CREATE TABLE " + RaccoltaTableContents.TABLE_NAME_RACCOLTA + " (" +
                            RaccoltaTableContents._ID + " INTEGER PRIMARY KEY," +
                            RaccoltaTableContents.COLUMN_LAT + DOUBLE_TYPE + COMMA_SEP +
                            RaccoltaTableContents.COLUMN_LON + DOUBLE_TYPE +
                            ")";

            private static final String SQL_CREATE_MAPPE =
                    "CREATE TABLE " + MappeTableContents.TABLE_NAME_MAPPE + " (" +
                            MappeTableContents._ID + " INTEGER PRIMARY KEY," +
                            MappeTableContents.COLUMN_NOME + STRING_TYPE + COMMA_SEP +
                            MappeTableContents.COLUMN_FLAG + INT_TYPE +
                            ")";

            private static final String SQL_CREATE_SERVER =
                    "CREATE TABLE " + ServerTableContents.TABLE_NAME_SERVER + " (" +
                            ServerTableContents._ID + " INTEGER PRIMARY KEY," +
                            ServerTableContents.COLUMN_INDIRIZZO + STRING_TYPE +
                            ")";

            private static final String SQL_DELETE_STATICLOSS =
                    "DROP TABLE IF EXISTS " + PesiTableContents.TABLE_NAME;

            private static final String SQL_DELETE_MAPPE =
                    "DROP TABLE IF EXISTS " + MappeTableContents.TABLE_NAME_MAPPE;

            private static final String SQL_DELETE_WAY =
                    "DROP TABLE IF EXISTS " + WayTableContents.TABLE_NAME_WAY;

            private static final String EMPTY_WAY =
                    "DELETE FROM" + WayTableContents.TABLE_NAME_WAY;

            private static final String EMPTY_STATICLOSS =
                    "DELETE FROM" + WayTableContents.TABLE_NAME_WAY;

            private static final String EMPTY_RACCOLTA =
                    "DELETE FROM" + RaccoltaTableContents.TABLE_NAME_RACCOLTA;

            private static final String EMPTY_SERVER =
                    "DELETE FROM" + ServerTableContents.TABLE_NAME_SERVER;




            @Override
            public void onCreate(SQLiteDatabase db) {
                try {
                    db.execSQL(SQL_CREATE_WAY);
                    db.execSQL(SQL_CREATE_STATICLOSS);
                    db.execSQL(SQL_CREATE_RACCOLTA);
                    db.execSQL(SQL_CREATE_MAPPE);
                    db.execSQL(SQL_CREATE_SERVER);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                try{

                    /*db.execSQL(SQL_DELETE_STATICLOSS);
                    db.execSQL(SQL_DELETE_WAY);

                    db.execSQL(SQL_CREATE_WAY);
                    db.execSQL(SQL_CREATE_STATICLOSS);*/

                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }




