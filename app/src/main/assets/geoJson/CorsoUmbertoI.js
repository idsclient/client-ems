{
	"type": "FeatureCollection",
	"features": [
		{
			"type": "Feature",
			"geometry": {
				"type": "MultiPoint",
				"coordinates": [], 
				"bbox": [-Infinity, -Infinity, Infinity, Infinity]
			},
			"properties": {
				"desc": "Generated by GPSies.com http://www.gpsies.com/"
			}
		},
		{
			"type": "Feature",
			"geometry": {
				"type": "MultiLineString",
				"coordinates": [[[13.727191686630247, 43.308769521789834, 5.0], [13.72719, 43.30877, 5.0], [13.72721, 43.30873, 5.0], [13.72722, 43.30873, 5.0], [13.72725, 43.30868, 5.0], [13.7273, 43.3086, 5.0], [13.72734, 43.30854, 6.0], [13.72738, 43.30849, 6.0], [13.72743, 43.30841, 6.0], [13.72747, 43.30834, 6.0], [13.72752, 43.30827, 6.0], [13.72757, 43.30819, 6.0], [13.72762, 43.30811, 7.0], [13.72765, 43.30806, 7.0], [13.72772, 43.30794, 7.0], [13.72773, 43.30794, 7.0], [13.72776, 43.30789, 7.0], [13.7278, 43.30782, 7.0], [13.72783, 43.30777, 8.0], [13.72788, 43.30769, 8.0], [13.72793, 43.30761, 8.0], [13.72797, 43.30754, 8.0], [13.72798, 43.30754, 8.0], [13.72802, 43.30747, 8.0], [13.72805, 43.30742, 8.0], [13.72808, 43.30737, 9.0], [13.72813, 43.3073, 9.0], [13.72817, 43.30722, 9.0], [13.72818, 43.30722, 9.0], [13.72822, 43.30714, 9.0], [13.72825, 43.30708, 9.0], [13.72826, 43.30707, 9.0], [13.72836, 43.3071, 9.0], [13.72835, 43.3071, 9.0], [13.72845, 43.30713, 9.0], [13.72844, 43.30713, 9.0], [13.72858, 43.30717, 8.0], [13.72857, 43.30717, 8.0], [13.72871, 43.30721, 7.0], [13.7287, 43.30721, 7.0], [13.72886, 43.30726, 7.0], [13.72885, 43.30726, 7.0], [13.72894, 43.30729, 6.0], [13.72893, 43.30729, 6.0], [13.72899, 43.3073, 6.0], [13.72898, 43.3073, 6.0], [13.72907, 43.30733, 6.0]]],
				"bbox": [13.72907, 43.30877, 13.72719, 43.30707]
			},
			"properties": {
				"name": "corso umberto i",
				"desc": "Generated by GPSies.com http://www.gpsies.com/"
			}
		}
	]
}
