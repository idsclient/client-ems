var flag=false;     //flag per il pulsante per inserire l'indirizzo e posizionarsi con il TAP

var tilesFolder;	//folder delle tiles
var raccolta=new Array();	//array contenente i punti di raccolta
var nrac=0;	//indice che indica il punto di raccolta selezionato nell'ultimo path calcolato
var pathLayer=null;	//layer conentente il path calcolato
var inizioLayer=null;	//layer del path che unisce il cursore col primo punto del path calcolato
var fineLayer=null;		//layer del path che unisce l'ultimo punto del path con il punto di raccolta
var overlayPunti=new Array();	//array degli overlay dei punti tower del path calcolato
var path=null;
var is_running = false;
var oldPos=null;


var italyLayer = new ol.layer.Tile({
	source: new ol.source.OSM({
		url: 'tilesitaly/{z}/{x}/{y}.png'
	})
});


var map = new ol.Map({
        target: 'map',
        layers: [italyLayer],
        view: new ol.View({
        	center: ol.proj.transform([12.239, 42.407], 'EPSG:4326', 'EPSG:3857'),
          	zoom: 5,
         	minZoom:	5,
         	maxZoom:	6,
          //extent: [1229905.2619761887,4921472.885920532,1493901.8467337242,5075915.6201720005]
          	extent:[1090336.9664340306,4890007.785923029,1731131.49204331,5500564.647319642]
        })
});

//tolgo lo zoom automatico del dblclick
map.on('dblclick',dblclick);
var inter=map.getInteractions();
var dbl=inter.item(1);
map.removeInteraction(dbl);


/*
map.getView().on('change:center',function(){
	Android.showToast(map.getView().getCenter()[0]+" | "+map.getView().getCenter()[1]);
});
*/

/*
var deviceOrientation=new ol.DeviceOrientation({
	tracking: true
});
var HeadingOld=null;
var RotationOld=null;
setInterval(function(){
	var Heading=deviceOrientation.getHeading();
	var Rotation=map.getView().getRotation();
	if(HeadingOld==null){
		HeadingOld=Heading;
		RotationOld=Rotation;
		Android.showToast("Heading="+Heading+" Rotation="+Rotation);
	}else{
		var diffH=Heading-HeadingOld;
		var diffR=Rotation-RotationOld;
		Android.showToast("diffH="+diffH+" diffR="+diffR);
		map.getView().setRotation(diffH);
	}


},10000);
*/
var HeadingOld=0;
var RotationOld=null;
var cost=0;
function orientazione(lat1,lon1,lat2,lon2){
	var Heading = getBearing(lat1,lon1,lat2,lon2);
	  Android.showToast("Heading nuovo :"+Heading);
	  Android.showToast("Heading vecchio : "+HeadingOld);

    HeadingOld = Heading;
    //cost=cost + (Math.PI)/2;
    map.getView().setRotation(Heading);
}


function radians(n) {
  return n * (Math.PI / 180);
}
function degrees(n) {
  return n * (180 / Math.PI);
}

function getBearing(startLat,startLong,endLat,endLong){
  var distance = Math.sqrt( Math.pow(startLat-endLat,2) + Math.pow(startLong-endLong,2));
  var angolo = Math.asin((startLong - endLong) / distance);
  if((angolo < Math.PI && angolo >= (Math.PI)/2) || (angolo >= (3/4*Math.PI) && angolo >= Math.PI)){
        angolo = angolo *-1;
    }
  return angolo;
}

// convert radians to degrees
function radToDeg(rad) {
  return rad * 360 / (Math.PI * 2);
}
// convert degrees to radians
function degToRad(deg) {
  return deg * Math.PI * 2 / 360;
}
// modulo for negative values
function mod(n) {
  return ((n % (2 * Math.PI)) + (2 * Math.PI)) % (2 * Math.PI);
}






//////FUNZIONI PER INIZIALIZZARE LA MAPPA/////////////////////////////////////////////////////////////////

//setto la cartella contenente le tiles della mappa
function setTilesFolder(folder){
	tilesFolder=folder+'/{z}/{x}/{y}.png';
}
//inizializzo la mappa con la posizione desiderata
function initPos(lat, lon){
	var mapLayer = new ol.layer.Tile({
		source: new ol.source.OSM({
			url: tilesFolder
		})
	});
	map.addLayer(mapLayer);
	map.setView(new ol.View({
    	center: ol.proj.transform([lat, lon], 'EPSG:4326', 'EPSG:3857'),
        zoom: 17,
        minZoom:	9,
        maxZoom:	19
    }));
}

function centerView(lon, lat){
    map.getView().setCenter(ol.proj.transform([lon,lat], 'EPSG:4326', 'EPSG:3857'));
    map.getView().setZoom(17);
}


////// Puntatori delle POSIZIONI //////////////////////////////////////////////////////////////////////////////////
var oldP = new ol.Overlay({	 element:document.getElementById('old')     });
map.addOverlay(oldP);



var cursore = new ol.Overlay({	 element:document.getElementById('cursore')     });
map.addOverlay(cursore);
//////Icone statiche da mettere sopra la mappa//////////////////////////////////////////////////////////////////
//ICONE GPS e NETWORK
var icone= new ol.control.Control({    element: document.getElementById('icone')	});
map.addControl(icone);

//ICONA "centra"
var iconacentra= new ol.control.Control({
        element: document.getElementById('centra'),
        trigger:centraMappa
      });

map.addControl(iconacentra);


//////FUNZIONI//////////////////////////////////////////////////////////////////////////////////////

function GPSOn(){
    document.getElementById('iconaGPSon').style.display= 'block';
    document.getElementById('iconaGPS').style.display= 'none';
}
function GPSOff(){
    document.getElementById('iconaGPSon').style.display= 'none';
    document.getElementById('iconaGPS').style.display= 'block';
}
function NetworkOn(){
    document.getElementById('iconaNetwork').style.display= 'none';
    document.getElementById('iconaNetworkOn').style.display= 'block';

}
function NetworkOff(){
    document.getElementById('iconaNetworkOn').style.display= 'none';
    document.getElementById('iconaNetwork').style.display= 'block';
}
function centraMappa(){
    var p=cursore.getPosition();
    if(p!=undefined){
        map.getView().setCenter(p);
    }
}


//aggiungo la posizione tramite dblclick////////////////////
function dblclick(event){
    if(flag){
        cursore.setPosition(event.coordinate);
        cursore.setPositioning("center-center");
        centraMappa();
        var coord=ol.proj.transform(event.coordinate, 'EPSG:3857','EPSG:4326');
        resetPath();
        Android.getTAPPosition(coord[0],coord[1]);
    }
}
function setFlag(val){    flag=val;     }


//funzione per recuperare i punti del percorso calcolato con Dijkstra
function getPunti(){
    var p=Android.getPunti();
    var obj=JSON.parse(p);
    var percorso=obj.punti;
    rotation(percorso);
    path=percorso.slice();
    //Android.showLog("path.length="+path.length);
    //Android.showLog("percorso.length="+percorso.length);

    for (var i = 0; i < percorso.length; i++) {
        percorso[i]=ol.proj.transform([percorso[i][1],percorso[i][0]], 'EPSG:4326','EPSG:3857');
    }
    resetPath();
    vispercorso(percorso);
    if(is_running==true){
        simulation();
    }

}
//funzione per visualizzare il percorso calcolato
function vispercorso(percorso){

    var featureRed = new ol.Feature({
        geometry: new ol.geom.LineString(percorso)
	});
	var vectorRedLine = new ol.source.Vector({});
	vectorRedLine.addFeature(featureRed);
	var vectorRedLayer = new ol.layer.Vector({
    	source: vectorRedLine,
    	style: new ol.style.Style({
    		stroke: new ol.style.Stroke({
           		color: '#FF0000',
       	    	width: 4
    	    }),
    	    fill: new ol.style.Fill({
    		    color: '#FF0000',
    		    weight: 1
    	    })
    	})
	});
	map.addLayer(vectorRedLayer);
	pathLayer=vectorRedLayer;

	//aggiungo i pezzi tratteggiati
	var inizio=new Array();
	inizio[0]=cursore.getPosition();
	inizio[1]=percorso[0];
	var featureBlu = new ol.Feature({
		geometry: new ol.geom.LineString(inizio)
    });
    var vectorBluLine = new ol.source.Vector({});
    vectorBluLine.addFeature(featureBlu);
    var vectorBluLayer = new ol.layer.Vector({
        source: vectorBluLine,
        style: new ol.style.Style({
        	stroke: new ol.style.Stroke({
           		color: '#0000FF',
            	width: 4,
            	lineDash: [20,20]
            }),
            fill: new ol.style.Fill({
        	    color: '#0000FF',
        	    weight: 1
            })
        })
    });
    map.addLayer(vectorBluLayer);
    inizioLayer=vectorBluLayer;

    var fine=new Array();
    fine[0]=percorso[percorso.length-1];
    fine[1]=raccolta[nrac];
    var featureBlu2 = new ol.Feature({
    		geometry: new ol.geom.LineString(fine)
        });
        var vectorBluLine2 = new ol.source.Vector({});
        vectorBluLine2.addFeature(featureBlu2);
        var vectorBluLayer2 = new ol.layer.Vector({
            source: vectorBluLine2,
            style: new ol.style.Style({
            	stroke: new ol.style.Stroke({
               		color: '#0000FF',
                	width: 4,
                	lineDash: [20,20]
                }),
                fill: new ol.style.Fill({
            	    color: '#0000FF',
            	    weight: 1
                })
            })
        });
        map.addLayer(vectorBluLayer2);
        fineLayer=vectorBluLayer2;
}


/////////////////////////////////////////////////



function setCursorPosition(lon,lat){
    Android.showLog("sposto cursore in "+lat+" "+lon);
    if(cursore.getPosition()==null || cursore.getPosition()==undefined){}
    else{
        oldPos=cursore.getPosition();
        //oldP.setPosition(oldPos);
    }

	cursore.setPosition(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'));
	cursore.setPositioning("center-center");
	map.getView().setCenter(ol.proj.transform([lon, lat], 'EPSG:4326', 'EPSG:3857'));
	rotationGPS();
}

var data = [[13.727052211761475, 43.31081492879186, 6.0], [13.72706, 43.31079, 6.0], [13.72695, 43.31076, 6.0], [13.72695, 43.31075, 6.0], [13.72679, 43.3107, 6.0], [13.72678, 43.3107, 6.0], [13.72666, 43.31065, 6.0], [13.72665, 43.31065, 6.0], [13.72644, 43.31057, 6.0], [13.72643, 43.31057, 6.0], [13.7263, 43.31052, 6.0], [13.7263, 43.31051, 6.0], [13.72607, 43.31043, 6.0], [13.72605, 43.31046, 6.0], [13.72599, 43.31055, 6.0], [13.72591, 43.31067, 6.0], [13.72586, 43.31074, 6.0], [13.7258, 43.31083, 6.0], [13.72573, 43.31092, 6.0], [13.72568, 43.31099, 5.0], [13.72561, 43.31109, 5.0], [13.72553, 43.3112, 5.0], [13.72554, 43.3112, 5.0], [13.72547, 43.31129, 4.0], [13.72548, 43.31129, 4.0], [13.72539, 43.31138, 4.0], [13.72532, 43.31153, 4.0], [13.72524, 43.31168, 4.0], [13.72519, 43.31159, 3.0]];
var pointOld = null;
var pointList;

/*function simulation(){
	//getBearing(43.31081492879186,13.727052211761475, 43.31159,13.72519);
    Android.showToast("length: "+path.length);
	if(path.length >0){
		var point = path.shift();
		//Android.showToast(point[1]+" "+point[0]);
        //Android.showToast(point[0]+" "+point[1]);
        setCursorPosition(point[0],point[1]);
        if(pointOld!=null){
        		//orientazione(pointOld[1],pointOld[0],point[1],point[0]);
        }
        pointOld = point;
        getTAPPosition();
        setTimeout(simulation,1500);
	}
}
*/

function simulation(){
 	//getBearing(43.31081492879186,13.727052211761475, 43.31159,13.72519);
    Android.showLog("Length: "+path.length);
 	if(path.length >0){
 		var point = path.shift();
 		//Android.showToast(point[1]+" "+point[0]);
         //Android.showToast(point[0]+" "+point[1]);
         cursore.setPosition(ol.proj.transform([point[1], point[0]], 'EPSG:4326', 'EPSG:3857'));
         cursore.setPositioning("center-center");
         map.getView().setCenter(ol.proj.transform([point[1], point[0]], 'EPSG:4326', 'EPSG:3857'));

         if(pointOld!=null){
         		//orientazione(pointOld[1],pointOld[0],point[1],point[0]);
         }
         pointOld = point;
         if(path.length>0){
            pointEnd=path.shift();
            sleep(500);
            Android.getTAPPosition(pointEnd[1],pointEnd[0]);
            //animation(point,pointEnd);
         }
         //Android.showLog("Adesso");
         else{
            Android.swChosed();
         }
 	}
 }

//aggiungo e visualizzo i punti di raccolta:
function addPuntoRaccolta(lat,lon){
	var l=raccolta.length;
	raccolta[l]=new Array();
	raccolta[l][0]=lon;
	raccolta[l][1]=lat;
}
function visRaccolta(){
	var html="";
	for(var i=0;i<raccolta.length;i++){
		raccolta[i]=ol.proj.transform(raccolta[i], 'EPSG:4326', 'EPSG:3857')
		var el="<img id='raccolta"+i+"' src='icon/greenflag.png' height='40' width='40' >";
        html=html+el;
    }
    document.getElementById('raccolta').innerHTML = html;
	for (var i = 0; i < raccolta.length; i++) {
		var punto = new ol.Overlay({ element: document.getElementById('raccolta'+i)	});
		map.addOverlay(punto);
		punto.setPosition(raccolta[i]);
		punto.setPositioning("center-center");
	}
}


//funzioni per recuperare i punti filtrati del percorso calcolato con Dijkstra
function getPunti2(){
    var p=Android.getPunti2();
    var obj=JSON.parse(p);
    var percorso=obj.punti;
    for (var i = 0; i < percorso.length; i++) {
        percorso[i]=ol.proj.transform([percorso[i][1],percorso[i][0]], 'EPSG:4326','EPSG:3857');
    }
    disegnaPunti(percorso);
}

function disegnaPunti(percorso){
 	var html;
 	var el;
 	for (var i = 0; i < percorso.length; i++) {
         el="<img id='punto"+i+"' src='icon/loader1.gif' height='20' width='20' onclick='alertNodo("+i+");'>";
         html=html+el;
     }
     if(html!=undefined)
     document.getElementById('punti').innerHTML = html;

     for (var i = 0; i < percorso.length; i++) {
     	var punto = new ol.Overlay({ element: document.getElementById('punto'+i)	});
 		map.addOverlay(punto);
 		punto.setPosition(percorso[i]);
 		punto.setPositioning("center-center");
 		overlayPunti[i]=punto;
     }
 }


function alertNodo(n){
 	var pos=overlayPunti[n].getPosition();
 	var coord=ol.proj.transform(pos, 'EPSG:3857','EPSG:4326');
    Android.getTAPPosition(coord[0],coord[1]);
    cursore.setPosition(pos);
    map.getView().setCenter(pos);
}

//resetto la mappa dal path calcolato
 function resetPath(){
 	if(pathLayer!=null){
 	    map.removeLayer(cursore);
 		map.removeLayer(pathLayer);
 		map.removeLayer(inizioLayer);
 		map.removeLayer(fineLayer);
 		pathLayer=null;
 		inizioLayer=null;
 		fineLayer=null;
 	}
 	for(var i=0;i<overlayPunti.length;i++){
 		map.removeOverlay(overlayPunti[i]);
 	}
 	overlayPunti=new Array();
 	document.getElementById('punti').innerHTML = "";
 	document.getElementById('raccolta').innerHTML = "";
 	//stopSimulation();
 	//setFlag(false);
 	//oldPos=null;
 }

//setto l'indice del punto di raccolta selezionato dal path calcolato
function setNrac(n){ 	nrac=n;  }

function startSimulation(){
    is_running = true;
}
function stopSimulation(){
    is_running = false;
}
function animation(point1,point2){
var dx= (point2[1]-point1[1])/5;
var dy= (point2[0]-point1[0])/5;
for (var i=0;i<5;i++)
  {sleep(200);
   point1[1]+=dx;
    point1[0]+=dy;
    setCursorPosition(point1[1],point1[0]);
    Android.showLog("Setto la posizione intermedia a : "+point1[1] + " " +point1[0] );


    }

}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function rotation(perc){
    var rot=0;
    var arct=0;
    for(var i=0;i<perc.length;i++){
        var posC=ol.proj.transform(cursore.getPosition(), 'EPSG:3857','EPSG:4326');
        var diffLat=posC[1]-perc[i][0];
        var diffLon=posC[0]-perc[i][1];

        //controllo solo differenze maggiori di 1 metro
        if(Math.abs(diffLat)<0.00001 && Math.abs(diffLon)<0.00001){
            continue;
        }else{
            var x=diffLon;
            var y=diffLat;
            arct=Math.atan2(y,x);

            if(x>0 && y>0){//quadrante 1
                rot=arct+Math.PI/2;
            }
            if(x<0 && y>0){//quadrante 2
                rot=arct+Math.PI/2;
            }
            if(x<0 && y<0){//quadrante 3
                rot=arct+5/2*Math.PI;
            }
            if(x>0 && y<0){//quadrante 4
                rot=arct+Math.PI/2;
            }
            break;
        }
    }

    var anim=new ol.animation.rotate({
        rotation: map.getView().getRotation()
    });
    map.beforeRender(anim);

    map.getView().setRotation(rot);
}



function rotationGPS(){
    if(oldPos!=null){
        var rot=0;
        var arct=0;

        var posC=ol.proj.transform(cursore.getPosition(), 'EPSG:3857','EPSG:4326');
        var oldPosCoord=ol.proj.transform(oldPos, 'EPSG:3857','EPSG:4326');

        var diffLat=oldPosCoord[1]-posC[1];
        var diffLon=oldPosCoord[0]-posC[0];
        //controllo solo differenze maggiori di 1 metro
        if(Math.abs(diffLat)<0.00005 && Math.abs(diffLon)<0.00005){
            return;
        }else{
            var x=diffLon;
            var y=diffLat;
            arct=Math.atan2(y,x);

            if(x>0 && y>0){//quadrante 1
                rot=arct+Math.PI/2;
            }
            if(x<0 && y>0){//quadrante 2
                rot=arct+Math.PI/2;
            }
            if(x<0 && y<0){//quadrante 3
                rot=arct+5/2*Math.PI;
            }
            if(x>0 && y<0){//quadrante 4
                rot=arct+Math.PI/2;
            }

        }


        var anim=new ol.animation.rotate({
            rotation: map.getView().getRotation()
        });
        map.beforeRender(anim);
        map.getView().setRotation(rot);
    }
}
